//Definir el pin de control de nuestro motor
const int motPin=D3;

void setup() {
    //Establecer el pin de control como salida.
    pinMode(motPin,OUTPUT);
    /*
    * Asignarle el nombre "motor" a una función en la nube particle y
    * conectar la función motor (definida al final del archivo) a este
    * nombre.
    */
    Particle.function("motor",motor);
}

void loop() {
    //El loop no hace nada, todo lo maneja Particle.function()
}

int motor(String command){
    //Si la función recibe un parámetro "ON" prende el motor.
    if(command=="ON"){
        digitalWrite(motPin,1);
        return 1; //Función exitosa
    //Si la función recibe un "OFF" apaga el motor.
    }else if(command=="OFF"){
        digitalWrite(motPin,0);
        return 1;
    }
    //Cualquier otra entrada apaga el motor
    else{
        digitalWrite(motPin,0);
        return -1; //Funcion fracasada
    }
}