/*
   Controlar un electrodomestico con un rele
   Por: Hugo Vargas
   Fecha: 26 de marzo de 2020

   Este programa activa y desactiva un relevador por Internet, para 
   ello es necesario un panel de control en NodeRed, la comunicación
   Será por MQTT

   Configuración de hardware
    Relé                          NodeMCU
    GND ------------------------- GND
    VCC ------------------------- Vin
    S   ------------------------- D1

*/

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "AXTEL XTREMO-18D6";  // Nombre de red
const char* password = "038C18D6";  // Contraseña de la red

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.15.3";
IPAddress server(192, 168, 15, 3);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
int rele = D1;  // rele conectado a D1

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
  // Inicialización del programa
  // INicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Configuración de pines
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  pinMode(rele, OUTPUT);  // Inicializa el rele como input con resistencia Pull-Up interna

  // Condiciones Iniciales
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);
  digitalWrite (rele, HIGH);  // Se inicia con el relé apagado

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED) {
    digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT
}// Fin del void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  
}// Fin del void setup()

//----------Funciones de usuario

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false

    if (String(topic) == "codigoiot/output") {  // Si se recibe el mensaje true, se enciende el led y el rele
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
      digitalWrite(rele, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led y el rele
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
      digitalWrite(rele, HIGH);
    }// fin de else if(messageTemp == "false")
    }// fin de if (String(topic) == "codigoiot/output")
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("CodigoIoTClient")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("codigoiot/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}