/* Lectura de sensor de flama
 *  Por: Hugo Vargas
 *  Fecha: 6 de marzo de 2020
 *  
 *  Este programa manda a través de MQTT la lectura del nivel de ruido detectado
 *  por el sensor de audio. Para realizar la estimación de la amplitud
 *  de audio detectada, se promedia el nivel de lecturas por ensima del valor
 *  bias y el primedio del nivel de lecturas por debajo del valor  bias. La diferencia
 *  entre estos dos valores es la amplitud deportada. La forma en la que se calculan
 *  las diferencias en el programa, ayuda a garantizar que la lectura sea siempre positiva
 *  y a que siga existiendo un resultado racional en caso de que se indeterminen 
 *  los calculos de alguna mitad a causa de ligeras variaciones en el nivel de bias.
 *  
 *  Configuración del hardware;
 *  Sensor de audio      NodeMCU
 *  Vcc   -------------- 3.3V
 *  GND    -------------- GND
 *  OUT    -------------- A0
 */

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "nombre_de_red";  // Nombre de red
const char* password = "contraseña";  // Contraseña de la red

// Constantes
int INTERVAL = 1000;  // Determina el tiempo entre lecturas no bloqueantes
int SAMPLES = 256;

// Dirección de Broker MQTT. No olvides poner la IP de tu broker
const char* mqtt_server = "192.168.1.135";
IPAddress server(192,168,1,135);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

// Variables
int audioPin = A0;  // Esta variable señala el pin donde se conecta la salida de audio
long timeNow, timeLast;  // Control de acciones asincronas
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266

//---------Inicialización del programa, estas instrucciones se ejecutan una vez al energizar el sistema
void setup () {
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
  Serial.println ("Programa iniciado");

  // Configuración de pines
  pinMode (audioPin, INPUT);  // Lectura de nivel de audio
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED){
  digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT

  // Inicia seguimiento de eventos temporizados no bloqueantes
  timeLast = millis ();
}// fin del void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {

  timeNow = millis();

  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Control de eventos temporizados no bloqueantes
  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo

    long posLecture = 0;
    long negLecture = 0;  //Variables que guardan los niveles de audio registrados
    long posCounter = 0;
    long negCounter = 0;  // Variables que llevan la cuenta de lecturas positivas y negativas
    
    //lectura de nivel de ruido
    for (int i = 0; i < SAMPLES; i++)  // Se promedian varias ecturas para obtener un mejor resultado
    {
      int dataAudio = analogRead (audioPin);  // Realiza la lectura analogica del microfono
      if (dataAudio > 557) {  // Si la lectura pasa del punto medio, sumar la diferencia a contador positivo
        posLecture += dataAudio - 557;
        posCounter++;
      } else {  // Si la lectura esta por debajo del punto medio, sumar la diferencia a contador negativo
        negLecture += 556 - dataAudio;
        negCounter++;
      }// fin de if (dataAudio > 512)
    }

    //Se calculan los primedips
    float avgPos = (float)posLecture/posCounter;
    float avgNeg = (float)negLecture/negCounter;
    //Se calcula la amplitud 
    float amplitude = avgPos + avgNeg;
    
    // Mostrar los datos por seial
    Serial.print ("amplitud estimada");
    Serial.println (amplitude);

    // Enviar lectura por MQTT
    char dataString [8];
    dtostrf (amplitude, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("esp32/noise",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT

  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ();

//------------- Funciones de usuario
// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
   if (String(topic) == "esp32/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "esp32/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}