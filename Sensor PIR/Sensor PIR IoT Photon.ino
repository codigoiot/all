/*
   Sensor de presencia PIR
   Por: Hugo Vargas
   Fecha: 26 de marzo de 2020

   Este programa manda por la nube particle la lectura del sensor PIR y se visualiza en un
   flow en NodeRed.

   Configuración de hardware
    Sensor PIR                    NodeMCU
    VCC ------------------------- 3.3v
    GND ------------------------- GND
    S   ------------------------- 1

*/

// Variables del prorgama
int pirPin = D0;  // Sensor PIR conectado a D0
int led = D7;  // LED del Photon es D7

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
  pinMode(pirPin, INPUT);  // Inicializa el Pin D0 como input con resistencia Pull-Up interna
  pinMode(led, OUTPUT);  // Inicializa el Pin D7 como salida
}// fin del void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
  int pirValState;  // Variable para detectar el estado del sensor

  pirValState = digitalRead(pirPin);  // Lectura del sensor

  if(pirValState == HIGH) {  // Se detecto movimiento?
    digitalWrite(led, HIGH);  // Prende el LED
    delay(1000);  // Espera 5 segundos para hacer otra lectura
    Particle.publish("presencia","detectado",60,PRIVATE);  // Reporta la lectura a la nube particle
  }// fin del if(pirValState == HIGH)
  else  // En caso de que se detecte que no hay presencia
  {
    digitalWrite(led, LOW);  // Apaga el LED
    Particle.publish("presencia","noDetectado",60,PRIVATE);  // Reporta la lectura a la nube particle
  }// fin del else
}// Fin del void loop()