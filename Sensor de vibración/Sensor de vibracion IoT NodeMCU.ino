/*Lectura de sensor de alcohol
 * Por: Hugo Vargas
 * Fecha: 11 de marzo de 2020
 * 
 * Este es el prorgama básico de IOT con NodeMCU CP2102, MQTT y Node-Red.
 * Este programa realiza una lectura digital del pin donde está conectado
 * el sensor de vibración y hace un calculo aproximado de la frecuencia
 * detectada. Este programa reporta el valor de la frecuencia por Internet
 * a través de MQTT a un servidor en Node-Red que se encarga de desplegar
 * un tablero con la lectura.
 * 
 * Configuración de hardware
 *  Sensor de vibración         NodeMCU
 *  - ------------------------- GND
 *  + ------------------------- 3.3v
 *  s ------------------------- D2
 * 
*/

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Constantes
uint8_t vibration = D2;  // Pin en el que se conecta el sensor y es compatible con interrupciones por hardware

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "AXTEL XTREMO-1CE9";  // Nombre de red
const char* password = "037E1CE9";  // Contraseña de la red

// Constantes del programa
const int INTERVAL = 1000;  //Tiempo en milisegundos entre lecturas

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.15.3";
IPAddress server(192,168,15,3);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
long timeNow, timeLast;  // Control de acciones asincronas
bool set = 0;  // Variable que indica si hay que reportar la detección de la interrupción
int count = 0;  // Contador para conocer el numero de impactos detectados

//Configuración de RAM
void ICACHE_RAM_ATTR updateState ();  // Función para colocar la función de interrupción en la RAM y poder accederla

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  // Inicialización del programa
  // INicia comunicación serial
  Serial.begin (115200);  
  while(!Serial){};

  //Configuración de pines
  pinMode (vibration, INPUT_PULLUP);  // Pin que detectará la interrupción
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Configuración de interrupciones: pin, función ISR, modo
  attachInterrupt(digitalPinToInterrupt(vibration), updateState, FALLING);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED){
  digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);
  
  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT

  // Inicio de control de eventos asincronos
  timeLast = millis ();
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Funciones temporizadas no bloqueantes
  timeNow = millis();

  // Configuración de interrupciones: pin, función ISR, modo
  attachInterrupt(digitalPinToInterrupt(vibration), updateState, FALLING);
  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo

    // Enviar lectura por MQTT
    char dataString [8];
    dtostrf (count, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("esp32/frec",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("Lectura de fecuencia: ");  // Se reporta también por serial
    Serial.println (count);
    
    count = 0;  // Actualiza el seguimiento de frecuencia
  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ()

//------------- Funciones de usuario

// Función del ISR para usar en la interrupción
  void updateState () {
    count++;  // Se realiza un incremento para conocer el numero de ciclos por segundo
  }

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
   if (String(topic) == "esp32/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "esp32/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}