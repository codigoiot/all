/*
   Detonar una alarma
   Por: Hugo Vargas
   Fecha: 25 de marzo de 2020

   Este programa obtiene la información de fecha y hora desde internet y activa el
   servomotor y el motoreductor a una hora específica.

   Servomotor              Photon
   GND ------------------- GND
   VCC ------------------- 3.3v
   S   ------------------- D2

   Motorreductor           Photon
   +   ------------------- D3

*/

// Bibliotecas
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <Servo.h>

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "AXTEL XTREMO-18D6";  // Nombre de red
const char* password = "038C18D6";  // Contraseña de la red
Servo servo1;  // Crear el objeto que controla el servomotor

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.15.3";
IPAddress server(192, 168, 15, 3);

// Constantes del programa
static const char ntpServerName[] = "us.pool.ntp.org";  // Selecciona el servidor de tiempo
//static const char ntpServerName[] = "time.nist.gov";
//static const char ntpServerName[] = "time-a.timefreq.bldrdoc.gov";
//static const char ntpServerName[] = "time-b.timefreq.bldrdoc.gov";
//static const char ntpServerName[] = "time-c.timefreq.bldrdoc.gov";
const int timeZone = -5;  // Hora de la ciudad de México
//const int timeZone = 1;     // Central European Time
//const int timeZone = -4;  // Eastern Daylight Time (USA)
//const int timeZone = -8;  // Pacific Standard Time (USA)
//const int timeZone = -7;  // Pacific Daylight Time (USA)

//Objetos
WiFiUDP Udp;
unsigned int localPort = 8888;  // Escuchar paquetes UDP por el puerto 8888
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

// Ejecución de funciones de tiempo
time_t getNtpTime();
void digitalClockDisplay();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
int servoPin = D2;  // Declara el pin que maneja el servomotor
int reducerPin = D3;  // Declara el pin que maneja el motoreductor

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
  delay(250);

  //Configuración de pines
  pinMode (ledPin, OUTPUT);  // Configurar leds indicadores
  pinMode (ledPin2, OUTPUT);
  pinMode (reducerPin, OUTPUT);  // Configurar el pin que controla el motoreductor como salida

  //Condiciones Iniciales
  servo1.write(0);  // Inicializa en una posición de 0°
  digitalWrite (ledPin, HIGH);
  digitalWrite (ledPin2, HIGH);
  digitalWrite (reducerPin, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED) {
    digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Obtención de hora
  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Puerto local: ");
  Serial.println(Udp.localPort());
  Serial.println("Esperando sincronizacion de tiempo");
  setSyncProvider(getNtpTime);
  setSyncInterval(300);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT
}

time_t prevDisplay = 0; // when the digital clock was displayed

//----------Cuerpo del programa, bucle principal
void loop()
{
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Imprimir hora
  if (timeStatus() != timeNotSet) {
    if (now() != prevDisplay) { //update the display only if time has changed
      prevDisplay = now();
      digitalClockDisplay();
    }// fin de if (now() != prevDisplay)
  }// fin de if (timeStatus() != timeNotSet)

  // Comparar hora para activar motores
  if (hour() == 18 && minute() == 12) // Ajustar la hora deseada para activar el los motores
  {
    servo1.attach(servoPin);
    servo1.write(180);  // Mover el servomotr a 180°
    delay(2000);  // Esperar 2 segundos
    servo1.write(0);  // Mover el servomotor a 0°
    delay(2000);  // Esperar 2 segundos

    delay(60000);  // para que el evento sólo se realice una vez durante el minuto seleccionado
  }// fin de if(Time.hour() == 18 && Time.minute() == 12)
}

//------------- Funciones de usuario
void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(".");
  Serial.print(month());
  Serial.print(".");
  Serial.print(year());
  Serial.println();
}

void printDigits(int digits)
{
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false

  if (String(topic) == "codigoiot/output") {  // Si se recibe el mensaje true, se activa la alarma
    Serial.print("Se recibio ");
    if (messageTemp == "true") {
      //Activar motor
      Serial.println ("Activar motor");
      digitalWrite (servoPin, HIGH);
    }// fin de if(messageTemp == "true")
    else if (messageTemp == "false") {
      //Apagar motor
      Serial.println ("Apagar motor");
      digitalWrite (servoPin, LOW);
    }// fin de else if (messageTemp == "false")
    else { // De no recibirse el mensaje true, se compara con un valor analógico
      servo1.write(messageTemp.toInt());
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "codigoiot/output")

}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("codigoiot/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}