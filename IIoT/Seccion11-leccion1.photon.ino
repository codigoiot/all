int pirPin = D0; // Sensor PIR conectado a D0
int led = D7; // LED del Photon es D7

void setup()
{
  pinMode(pirPin, INPUT); // Inicializa el Pin D0 como input con resistencia Pull-Up interna
  pinMode(led, OUTPUT); // Inicializa el Pin D7 como salida
}


void loop()
{
  int pirValState;

  pirValState = digitalRead(pirPin);

  if(pirValState == HIGH){  // Se detecto movimiento?
    digitalWrite(led, HIGH);  // Prende el LED
    delay(1000); // Espera 5 segundos para hacer otra lectura
  }
  else
  {
    digitalWrite(led, LOW);  // Apaga el LED
  }

}