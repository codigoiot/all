/////////////////////////////
// Definición de los Pines //
/////////////////////////////
const int RHT03_DATA_PIN = D3; // pin de datos del RHT03
const int LIGHT_PIN = A0; // salida análoga de la fotocelda
const int LED_PIN = D7; // LED para mostrar cuando el sensor esta siendo leido

////////////////////////
// Crear objeto RHT03 //
////////////////////////
RHT03 rht; // Esto crea un objeto RTH03 que usaremos para interactuar con el sensor

unsigned int minimumLight = 65536;
unsigned int maximumLight = 0;
float minimumTempC = 5505;
float maximumTempC = 0;
float minimumTempF = 9941;
float maximumTempF = 0;
float minimumHumidity = 100;
float maximumHumidity = 0;

#define PRINT_RATE 1500 // Pausa en ms entre las impresiones.

void setup() 
{
    // Serial.begin() es usado para abrir la interfase serial entre el Photon
    // y tu computadoraa.
    // El parametro '9600' configura la velocidad de la interfase. Este valor es
    // llamado "baud rate" (taza de transferencia), que es equivalente a bits por segundo (bps).
    Serial.begin(9600); // Abre la interfase serial a 9600 bps
    // Usando el objeto 'rht' creado en la sección global, empezamos por llamar
    // a su función 'begin'.
    // El parámetro en esta función es el PIN DIGITAL que usamos para comunicarnos
    // con el sensor.
    rht.begin(RHT03_DATA_PIN);  // Inicializamos el sensor RHT03
    
    // No olvides establecer los modos de los pines del sensor análogo como entrada    // y del LED como salida:
    pinMode(LIGHT_PIN, INPUT); // Pin de la fotocelda - Entrada
    pinMode(LED_PIN, OUTPUT); // Pin del LED - Salida
    digitalWrite(LED_PIN, LOW); // Apagar el LED
}

void loop() 
{
    digitalWrite(LED_PIN, HIGH); // Prender el LED -- Se prenderá cada ves que leamos datos del sensor.
    
    // Usamos la función 'update' del RHT03 para leer nuevos valores de humedad y temperatura del sensor.
    // Existe la posibiidad de que la lectura falle por lo que la función considera que
    // regresará el valor '1' si la actualización es exitosa, o un valor negativo si falla.
    int update = rht.update();
    
    if (update == 1) // Si la actualización fue exitosa, imprime las lecturas nuevas:
    {
        // Usa analogRead para obtener la lectura actual de la fotocelda:
        unsigned int light = analogRead(LIGHT_PIN);
        // Guardamos los valores mínimo y máximo de iluminación:
        if (light > maximumLight) maximumLight = light;
        if (light < minimumLight) minimumLight = light;
        
        // LA función 'humidity()' regresa la última lectura
        // de humedad relativa del RHT03.
        // Regresa un valor de punto flotante -- un porcentaje de RH entre 0-100.
        // SOLO LLAMA ESTA FUNCIÓN SI TUVO ÉXITO LA EJECUCIÓN DE rht.update()!.
        float humidity = rht.humidity();
        // Guardamos los valores mínimo y máximo de humedad
        if (humidity > maximumHumidity) maximumHumidity = humidity;
        if (humidity < minimumHumidity) minimumHumidity = humidity;
        
        // La función 'tempF()' regresa el valor actual de temperatura 
        // en grados farenheit del sensor RHT03.
        // Regresa una variable de punto flotante con el valor en grados Farenheit.
        // SOLO LLAMA ESTA FUNCIÓN SI TUVO ÉXITO LA EJECUCIÓN DE rht.update()!.
        float tempF = rht.tempF();
        // Guardamos los valores mínimo y máximo de tempF
        if (tempF > maximumTempF) maximumTempF = tempF;
        if (tempF < minimumTempF) minimumTempF = tempF;
        
        // `tempC()` trabaja igual que 'tempF()', pero regresa la temperatura en
        // grados Celsios.
        // SOLO LLAMA ESTA FUNCIÓN SI TUVO ÉXITO LA EJECUCIÓN DE rht.update()!.
        float tempC = rht.tempC();
        // Guardamos los valores mínimo y máximo de tempC
        if (tempC > maximumTempC) maximumTempC = tempC;
        if (tempC < minimumTempC) minimumTempC = tempC;
        
        
        // `Serial.print()` es usada para enviar datos desde el Photon a la computadora usando la interfase serial.
        // El parámetro que enviamos a 'print()' puede ser cualquier cosa, desde un String hasta un arreglo de caracters,
        // o un valor de punto flotante, entero, o prácticamente cualquier tipo de variable.
        Serial.print("Light: "); // Imprime "Light: "
        Serial.print(light); // Imprime la lectura de iluminación
        Serial.print(" ("); // Imprime " ("
        Serial.print(minimumLight); // Imprime el valor mínimo de iluminación
        Serial.print('/'); // Print a '/' -- las comillas sencillas indican que solo enviamos un caracter
        Serial.print(maximumLight); // Imprime el valor máximo de iluminación.
        Serial.println(") (min/max)"); // Termina la linea con ") (min/max)"
        // La línea completa se verá parecida a: "Light: 545 (8/791) (min/max)"
        
        // Imprime la temperatura en °C:
        // Ejemplo de impresión: "Temp: 24.9 °C (23.5/24.5) (min/max)"
        Serial.print("Temp: ");
        Serial.print(tempC, 1); // Imprime un valor de punto flotante, podemos indicar el número de decimales con el segundo parámetro 
        Serial.print(" ");
        // 'write()' se usa para enviar un valor SINGLE BYTE a través de la linea serial:
        Serial.write(248); // 248 es el valor ASCII del simbolo °.
        Serial.print("C (");
        Serial.print(minimumTempC, 1);
        Serial.print('/'); // Imprime una '/'
        Serial.print(maximumTempC, 1);
        Serial.println(") (min/max)");
        
        // Imprime la temperatura en °F:
        // Ejemplo de impresión: "Temp: 76.1 °F (74.3/76.1) (min/max)"
        Serial.print("Temp: "); // Imprime "Temp: "
        Serial.print(tempF, 1); // Imprime la variable tempF -- 1 decimal
        Serial.print(' ');      // Imprime un espacio
        Serial.write(248);      // Imprime el valor ASCII 248 (el simbolo °)
        Serial.print("F (");    // Imprime "F ("
        Serial.print(minimumTempF, 1); // Imprime la temperatura mínima -- 1 decimal point
        Serial.print('/');      // Imprime una '/'
        Serial.print(maximumTempF, 1); // Imprime la temperatura máxima -- 1 decimal point
        Serial.println(") (min/max)"); // Termina la línea con ") (min/max)"
        
        // Imprime la humedad relativa:
        // Ejemplo de impresión: "Humidity: 29.7 % (29.10/41.80) (min/max)"
        Serial.print("Humidity: ");
        Serial.print(humidity, 1);
        Serial.print(" %");
        Serial.print(" (");
        Serial.print(minimumHumidity, 1);
        Serial.print("/");
        Serial.print(maximumHumidity, 1);
        Serial.println(") (min/max)");
        
        Serial.println(); // Imprime una linea en blanco:
    }
    else // Si la actualización falló, damos tiempo a que se reestablezca el sensor:
    {
        Serial.println("Error reading from the RHT03."); // Imprime mensaje de error
        Serial.println(); // Imprime linea en blanco
        
        delay(RHT_READ_INTERVAL_MS); // El RHT03 necesita cerca de 1 segundo entre lecturas
    }
    digitalWrite(LED_PIN, LOW); // Apagamos el LED
    
    delay(PRINT_RATE); // pausamos 1 seg para permitir la lectura de los valores.
}