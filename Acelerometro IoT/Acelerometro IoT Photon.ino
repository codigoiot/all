MMA8452Q accel; // Objeto que maneja el acelerómetro
unsigned int stepCount = 0; // Contador de pasos
void setup() 
{
    // Configurar Pines
    pinMode(LED_PIN, OUTPUT); // Pin de led como salida
    digitalWrite(LED_PIN, HIGH); // Iniciar con el pin apagado
    
    accel.begin(SCALE_2G, ODR_50); //Iniciarlizar el acelerometro
    
    //Datos de configuración del acelerómetro
    byte threshold = 1; // 2 * 0.063g = 0.063g
    byte pulseTimeLimit = 255; // 0.625 * 255 = 159ms (max)
    byte pulseLatency = 64; // 1.25 * 64 = 640ms
    
    // Configuración de funcion de detección de pasos
    accel.setupTap(threshold, threshold, threshold, pulseTimeLimit, pulseLatency);
}
void loop() 
{
    // Buscar pasos
    byte tap = accel.readTap();
    if (tap != 0) // If there was a tap
    {
        stepCount++; // Increment stepCount
        toggleLED(); // Toggle the LED state
        Particle.publish("Steps", String(stepCount / 2));
    }
}
// Funciones de usuario
void toggleLED()
{
    static bool ledState = true;
    if (ledState)
    {
        digitalWrite(LED_PIN, HIGH);
        ledState = false;
    }
    else
    {
        digitalWrite(LED_PIN, LOW);
        ledState = true;
    }
}
Ready. 
 miPhoton 
 v1.5.2 