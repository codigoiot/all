//Definimos el pin D7 como control del rele
#define rele D7

void setup() {
    //Establecer D7 (rele) como salida
    pinMode(rele,OUTPUT);
    /*
    * Registrar una funcion en la nube con el 
    * nombre "rele" y asociarla a la función
    * "controlRele.
   */
    Particle.function("rele",controlRele);
}

void loop() {
    /*
    * No hace nada el loop, todo lo controlará
    * la función controlRele
    */
}

int controlRele(String estado) {
    /* Las funciones Particle siempre toman un 
    * String como parámetro y regresan un int.
    * En este caso regresará 1 cuando el comando 
    * sea "on" y un 0 para "off". Si falla la 
    * función regresa -1
    */

    if (estado=="on") {
        //Si el estado es on activa el rele
        digitalWrite(rele,HIGH);
        return 1;
    }
    else if (estado=="off") {
        //Si es off desactivalo
        digitalWrite(rele,LOW);
        return 0;
    }
    else {
        return -1;
    }
}