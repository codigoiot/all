/*Codigo IoT- Internet de las Cosas para la Industria - Sección 11*/

int magnetPin = D0; // Switch magnético conectado a D0
int led = D7;     // LED del Photon está en D7

void setup()
{
  pinMode(magnetPin, INPUT_PULLUP); // Inicializa D3 como entrada con pull-up interno
  pinMode(led, OUTPUT);           // Inicializa D7 como salida
}


void loop()
{
  int magnetValState;

  magnetValState = digitalRead(magnetPin);

  if(magnetValState == HIGH){      // Si se cerro el switch...
    digitalWrite(led, HIGH);     // ... prende el led
  }
  else //Si no...
  {
    digitalWrite(led, LOW);  // ... apaga el LED
  }
}