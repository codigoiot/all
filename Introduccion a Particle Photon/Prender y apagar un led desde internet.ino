int ledToggle(String command) {
    /* Particle.function siempre toma un string como un argumento y regresa un entero.
       Cómo podemos pasarle un string, significa que podemos darle los comandos de como deberá ser usada la función.
       En este caso, diciéndole que el comando "on" encenderá el LED y que el comando "off" lo apagará.
       Entonces, la función nos regresará un valor para dejarnos saber qué ocurrió.

       En este caso, la función regresará 1 cuando el LED se encienda, 0 cuando el LED se apague,
       y -1 si le pasamos un comando inválido y que no tiene efecto alguno sobre el LED.
       */

    if (command=="on") {
        digitalWrite(led1,HIGH);
        digitalWrite(led2,HIGH);
        return 1;
    }
    else if (command=="off") {
        digitalWrite(led1,LOW);
        digitalWrite(led2,LOW);
        return 0;
    }
    else {
        return -1;
    }
}