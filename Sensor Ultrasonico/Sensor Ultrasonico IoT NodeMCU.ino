/* Lectura de sensor de distancia utlrasónico por IoT
 *  Por: Hugo Vargas
 *  Fecha: 4 de marzo de 2020
 *  
 *  Este es un programa básico para obtener una lectura simple del sensor de distancia
 *  ultrasónicom esto quiere decir que se obtendra el valor de la distancia a la que 
 *  se encuentra un obstaculo frente al sensor.
 *  
 *  Este programa manda un puso de 10us y después escucha el pulso de respuesta del
 *  sensor, el cual tiene misma duración que el tiempo que tomo al eco regresar. Después
 *  se calcula la distancia a la que se encuentra el obstaculo y se envía por serial y
 *  lo reporta a través de MQTT a un servidor remoto. Para ello es necesario tener
 *  acceso a un broker MQTT y a un servidor en NodeRed que permita visualizar el resultado.
 *  
 *  Configuración del hardware;
 *  Sensor Ultrasónico  NodeMCU
 *  Vcc   -------------- 3.3V
 *  Trig  -------------- D1
 *  Echo  -------------- D2
 *  GND   -------------- GND
 */

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "INFINITUM3568_2.4";  // Nombre de red
const char* password = "0516672984";  // Contraseña de la red

// Constantes del programa
const int INTERVAL = 1000;  //Tiempo en milisegundos entre lecturas

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.1.135";
IPAddress server(192,168,1,135);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

// Variables
int trig = D1;  // Representa el pin con el que se activa el sensor
int echo = D2;  // Representa el pin en el que se lle a respuesta del sensor
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
long t;  // Variable que almacena el tiempo que tarda en regesar el eco
long d;  // Variable que almacena el resultado del calculo de la distancia
long timeNow, timeLast;  // Control de acciones asincronas

//---------Inicialización del programa, estas instrucciones se ejecutan una vez al energizar el sistema
void setup () {

  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
  Serial.println ("Programa iniciado");
  
  // Configuración de pines
  pinMode (trig, OUTPUT);  // Por el pin trig sale una señal eléctrica
  pinMode (echo, INPUT);  // Por el pin echo se recibe una señal eléctrica
  pinMode(A0,INPUT);  // Se configira el pin A0 como entrada
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED){
  digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT

  // Inicio de control de eventos asincronos
  timeLast = millis ();
}// fin del void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {

  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  timeNow = millis();
  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo

    digitalWrite (trig, HIGH);  // Se lanza el pulso que activa el sensor
    delayMicroseconds (10);  // Este pulso debe tener una duración de 10 us
    digitalWrite (trig, LOW);  // Se desactiva el pin para finalizar el pulso

    //Medir el tiempo que tarda en regresar el eco
    t = pulseIn (echo, HIGH);  // Se almacena en t el valor de duración del pulso
                               // Se hace con ayuda de la función pulseIn, la cual
                               // forma parte de las bibliotecas de arduino, cuenta
                               // con dos parámetros, el primero es el pin que se
                               // va a leer, el segundo es el tipo de pulsos que
                               // detecta y puede ser HIGH o LOW

    d = t/58.275;  // Esta formula obtiene la distancia al multiplicar por el factor
                     // de conversión de la velocidad del sonido en m/s a cm/uS para
                     // una velocidad del sonido de 343.2m/s

    // Enviar lectura por MQTT
    char dataString [8];
    dtostrf (d, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("esp32/ultrasonic",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("Lectura del sensor: ");  // Se reporta también por serial
    Serial.print (d);
    Serial.println ("cm");
  }// fin de if (timeNow - timeLast > INTERVAL)  
  
}// fin de void loop ();


//------------- Funciones de usuario
// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
   if (String(topic) == "esp32/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "esp32/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}