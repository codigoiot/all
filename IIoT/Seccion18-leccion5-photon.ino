 //Define los pines del potenciómetro, del botón y del LED indicador

#define potPin A0

#define connectPin D2

#define indicador D7

//////////////////
// CLIENTE MQTT //
/////////////////

/*
* Para usar ip se define así el servidor,
* byte server[] = { XXX,XXX,XXX,XXX };
* MQTT client(server, 1883, callback);
* Para usar nombre de domino:
* MQTT client("www.sample.com", 1883, callback);
*/

//Dirección IP del servidor, en este caso a la que tu equipo asigna a la red hospedada
byte server[] = { 192,168,136,1 };

/*
* Creación de un objeto MQTT con la dirección del servidor,
* el puerto y la función callback que se debe llamar cada vez
* que se reciba un mensaje.
*/

MQTT client(server, 1883, mqtt_callback);
char clientId[]="miNombre"; //Cambiar por el nombre del cliente que publica
char tema[]="seccion18/pot"; //Opcional, personalizar tema

/* Definición de una función callback. Esta función recibe automáticamente
* del cliente los parámetros topic (el tema MQTT), el payload (el mensaje que
* se recibió) y length (el largo del mensaje).
*/

void mqtt_callback(char* topic, byte* payload, unsigned int length) {

/*
* En este caso la función no hace nada porque nuestro cliente
* no está suscrito a ningún tema.
*/
return;
}

///////////////////////////////////////
// Variables para la lectura del pot //
///////////////////////////////////////

int valorPot;
int valorAnterior=0;
int diferencia;
//Notece que esto es char y no String
char charValue[4];

/////////////////////////
// CONEXIÓN A INTERNET //
/////////////////////////

/*
* Usar modo semi-automático para que el Photon se conecte
* a wi-fi pero no a la nube Particle. Esto hace que la
* comunicación sea sólo entre la red hospedada en tu PC y
* el Photon.
*/
SYSTEM_MODE(SEMI_AUTOMATIC);

void setup() {
  //Conectar a wi-fi
  WiFi.connect();
  waitFor(WiFi.ready,15000);

  //Modo de los pines
  pinMode(connectPin,INPUT_PULLUP);
  pinMode(potPin, INPUT);
  pinMode(indicador, OUTPUT);

  //Conectar al broker MQTT
  client.connect(clientId);
  //Si la conexión fue exitosa prender el LED
  if (client.isConnected()) {
    digitalWrite(indicador,HIGH);
  }
}

void loop() {
  //Si el cliente sigue conectado
  if (client.isConnected()){
    client.loop(); //Actualizarlo
    digitalWrite(indicador,HIGH); // y prender el led
  }

  else{ //Si no sigue conectado
    digitalWrite(indicador,LOW); // apagar el LED,
    client.disconnect(); // cerrar la conexión
    client.connect(clientId); // y conectar de nuevo
  }

  //Leer el valor del potenciometro
  valorPot=analogRead(potPin);

  //Calcular la diferencia con el valor anterior
  diferencia=abs(valorPot-valorAnterior);
  if(diferencia>=20){ //Si la diferencia es mayor a 20
    sprintf(charValue,"%d",valorPot); //convertirla a un arreglo de caracteres
    client.publish(tema,charValue); //publicarla al tema definido arriba

    //Actualizar el valorAnterior con el actual
    valorAnterior=valorPot;
    delay(300); //y esperar un tercio de segundo antes de tomar una lectura nueva
  }

  //Si se presiona el botón conectar a la nube Particle
  if (digitalRead(connectPin)==0){
    Particle.connect();
    waitFor(Particle.connected,30000);
  }
}