/*Lectura de sensor de gas LPG modelo MQ-6
 * Por: Hugo Vargas
 * Fecha: 2 de marzo de 2020
 * 
 * Este es el programa básico de IoT con NodeMCU CP2102, MQTT y Node-Red.
 * Este programa realiza una lectura analógica de la conductividad del sensor
 * MQ-6 para detección de gas LPG. Este programa reporta el valor del ADC
 * por internet a través de MQTT a un servidor en Node-Red que se encarga
 * de desplegar un tablero la lectura del sensor y desplegar notificaciones
 * por correo y sonoras. El sensor además despliega el valor del ADC por 
 * puerto serial
 * 
 * El NodeMCU AMICA con programador CP2102 cuenta con un puerto analogico de
 * 10 bits de resolucion reportando valores de 0 a 1024, los cuales son
 * convertidos en PPM en nodeRed.
 * 
 * Configuración de harware:
 * MPU-92250    NodeMCU
 * VDD -------- 3.3V
 * GND -------- GND
 * A0  -------- A0
 * 
*/

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Constantes
const int WARM_UP = 30;  // Espera 30 segundos a que la resistencia de calentamiento alcance temperatura nominal

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "INFINITUM3568_2.4";  // Nombre de red
const char* password = "0516672984";  // Contraseña de la red
// Constantes del programa
const int INTERVAL = 5000;  //Tiempo en milisegundos entre lecturas

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.1.135";
IPAddress server(192,168,1,135);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
long timeNow, timeLast;  // Control de acciones asincronas
int MQ6 = A0;  // Pin al que se conecta la salida analógica del sensor

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  // Inicialización del programa
  // INicia comunicación serial
  Serial.begin (115200);  
  while(!Serial){};

  // Espera a que se alcance temperatura nominal de resistencia de calentamiento
  Serial.println ();
  Serial.println ("Esperando temperatura nominal en resistencia de calentamiento");
  for (int i = 0; i < WARM_UP; i++) {
    Serial.print (".");
    delay (1000);
  }
  Serial.println ();
  Serial.println ("Temperatura nominal alcanzada, ya se pueden realizar lecturas");

  //Configuración de pines
  pinMode(A0,INPUT);  // Se configira el pin A0 como entrada
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED){
  digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);
  
  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Funciones temporizadas no bloqueantes
  timeNow = millis();
  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo

    //variables
    int sensorValue;
    // Lectura del sensor
    for (int i = 0 ; i < 100 ; i++) //Se toman 100 lecturas para promediar su valor y obtener el valor de la lectura
     {
       sensorValue = sensorValue + analogRead(A0);
     }// fin de for (int i = 0 ; i < 100 ; i++)
    sensorValue = sensorValue / 100.0; // Se realiza el promedio
    //Reporta por serial
    Serial.print("Lectura analogica ");
    Serial.println(sensorValue);

    // Enviar lectura por MQTT
    char dataString [8];
    dtostrf (sensorValue, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("esp32/lpg",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("Lectura del sensor: ");  // Se reporta también por serial
    Serial.println (sensorValue);
  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ()

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
   if (String(topic) == "esp32/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "esp32/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}
    