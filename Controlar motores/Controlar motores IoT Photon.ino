/*

Detonar una alarma
Por: Hugo Vargas
Fecha: 25 de marzo de 2020

Este programa obtiene la información de fecha y hora desde internet y activa el
servomotor y el motoreductor a una hora específica.

Adicionalmente, este programa permite el control de los servomotores desde un
panel en NodeRed

Servomotor              Photon
GND ------------------- GND
VCC ------------------- 3.3v
S   ------------------- D2

Motorreductor           Photon
+   ------------------- D3

*/

//Variables del programa
int servoPin = D2;  // Declara el pin que maneja el servomotor
int reducerPin = D3;  // Declara el pin que maneja el motoreductor

//Objetos
Servo servo1;  // Crear el objeto que controla el servomotor

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
    // Inicialización del programa
    // Inicia comunicación serial
    Serial.begin (115200);
    while (!Serial) {};
    
    //Configuración de pines
    pinMode (reducerPin, OUTPUT);  // Configurar el pin que controla el motoreductor como salida
    
    // Inicializaciónes
    servo1.attach(servoPin);  // Acopla el servomotor al pin D2
    Time.zone(-5);  // Ajustar la zona de tiempo. Encuentra tu zona en http://www.timeanddate.com/time/map/
    Particle.syncTime();  // Obtener la hora de Internet
    
    //Condiciones Iniciales
    servo1.write(0);  // Inicializa en una posición de 0°
    
    // Funciones Párticle
    Particle.function("reducer", reducerControl);  // Para detectar sensores digitales
    Particle.subscribe("servo", servoControl, MY_DEVICES);  // Para monitorear sensores analógicos
}// fin de void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
    // Comparar la hora para activar el servomotor
  if(Time.hour() == 18 && Time.minute() == 12)  // Ajustar la hora deseada para activar el los motores
  {
    servo1.attach(servoPin);
    servo1.write(180);  // Mover el servomotr a 180°
    delay(2000);  // Esperar 2 segundos
    servo1.write(0);  // Mover el servomotor a 0°
    delay(2000);  // Esperar 2 segundos

    delay(60000);  // para que el evento sólo se realice una vez durante el minuto seleccionado
  }// fin de if(Time.hour() == 18 && Time.minute() == 12)
}// fin del void loop()

//------------- Funciones de usuario

//Funcion que controla el motorreductor
int reducerControl (String command) {
    
    if (command=="on") {
        Serial.println ("Activar motor");
        digitalWrite (servoPin, HIGH);
        return 1;
    }// fin del if (command=="on")
    else if (command == "off") {
        Serial.println ("Apagar motor");
        digitalWrite (servoPin, LOW);
    }// fin del else if (command == "off")
    else {  // En caso contrario, manda un código de error
        return -1;
    }
}// fin del int funcionAlerta(String command)

// Funcion que controla el servomotor
void servoControl (const char *event, const char *data)
{
    int nData = String(data).toInt();  //Converison de char a string
    servo1.write (nData);  // Escribir en el servo el angulo
}