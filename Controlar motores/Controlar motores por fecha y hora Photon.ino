/*

Detonar una alarma
Por: Hugo Vargas
Fecha: 25 de marzo de 2020

Este programa obtiene la información de fecha y hora desde internet y activa el
servomotor y el motoreductor a una hora específica.

Servomotor              Photon
GND ------------------- GND
VCC ------------------- 3.3v
S   ------------------- D2

Motorreductor           Photon
+   ------------------- D3

*/

//Variables del programa
int servoPin = D2;  // Declara el pin que maneja el servomotor
int reducerPin = D3;  // Declara el pin que maneja el motoreductor

//Objetos
Servo servo1;  // Crear el objeto que controla el servomotor

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
    // Inicialización del programa
    // Inicia comunicación serial
    Serial.begin (115200);
    while (!Serial) {};
    
    //Configuración de pines
    pinMode (reducerPin, OUTPUT);  // Configurar el pin que controla el motoreductor como salida
    
    // Inicializaciónes
    servo1.attach(servoPin);  // Acopla el servomotor al pin D2
    Time.zone(-5);  // Ajustar la zona de tiempo. Encuentra tu zona en http://www.timeanddate.com/time/map/
    Particle.syncTime();  // Obtener la hora de Internet
    
    //Condiciones Iniciales
    servo1.write(0);  // Inicializa en una posición de 0°
}// fin de void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
    // Comparar la hora para activar el servomotor
  if(Time.hour() == 18 && Time.minute() == 12)  // Ajustar la hora deseada para activar el los motores
  {
    servo1.attach(servoPin);
    servo1.write(180);  // Mover el servomotr a 180°
    delay(2000);  // Esperar 2 segundos
    servo1.write(0);  // Mover el servomotor a 0°
    delay(2000);  // Esperar 2 segundos

    delay(60000);  // para que el evento sólo se realice una vez durante el minuto seleccionado
  }// fin de if(Time.hour() == 18 && Time.minute() == 12)
}// fin del void loop()