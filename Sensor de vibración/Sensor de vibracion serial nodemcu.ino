/*  Lectura serial del sensor de vibración
 *  Por: Hugo Vargas
 *  Fecha 11 de marzo de 2020
 *  
 *  Este es un programa básico para obtener una lectura simple del sensor
 *  de vibración, lo cual se realizará a través de interrupciones y mandará
 *  un mensaje por puerto serial. Este programa puede ser considerado como
 *  el detector de impacto.
 *  
 *  Configuración de hardware
 *  Sensor de vibración         NodeMCU
 *  - ------------------------- GND
 *  + ------------------------- 3.3v
 *  s ------------------------- D2
 */

// Constantes
uint8_t vibration = D2;  // Pin en el que se conecta el sensor y es compatible con interrupciones por hardware

// Variables
bool set = 0;  // Variable que indica si hay que reportar la detección de la interrupción
int count = 0;  // Contador para conocer el numero de impactos detectados

void ICACHE_RAM_ATTR updateState ();  // Función para colocar la función de interrupción en la RAM y poder accederla

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  // Configuración de pines
  pinMode (vibration, INPUT_PULLUP);

  // Configuración de interrupciones: pin, función ISR, modo
  attachInterrupt(digitalPinToInterrupt(vibration), updateState, FALLING);

  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {

  // En caso de detectarse la activación de las interrupciones
  if (set == 1) {
    count++;
    Serial.println (count);  // Se reporta la detección de impacto
    delay (500);  // Esta espera actua como antirebote
    set = 0;  // Se reinician las condiciones
  }// fin de if (set)
}// fin de void loop ()

//------------- Funciones de usuario

// Función del ISR para usar en la interrupción
void updateState () {
  set = 1;  // Se cambia el valor de esta variable a modo indicador para conocer que se ha detectado un impacto con el sensor de vibración
}