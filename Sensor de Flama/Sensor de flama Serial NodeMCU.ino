/* Lectura de sensor de flama por IoT
 *  Por: Hugo Vargas
 *  Fecha: 5 de marzo de 2020
 *  
 *  Este es un programa básico para obtener dos lectuas del sensor de flama, la
 *  lectura analogica y la lectura digital, la cual solo representa que se ha superado
 *  un umbral determinado fisicamente por el usuario. Las lecturas se visualizan
 *  en el monitor serial y se envían por MQTT a un broker, al cual se suscribe una
 *  instancia de NodeRed, permitiendote activar una alerta sonora y visualizar los
 *  valores de los sensores en una interfaz web.
 *  
 *  Configuración del hardware;
 *  Sensor de flama      NodeMCU
 *  Vcc   -------------- 3.3V
 *  A0    -------------- D1
 *  D0    -------------- D2
 *  GND   -------------- GND
 */

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "INFINITUM3568_2.4";  // Nombre de red
const char* password = "0516672984";  // Contraseña de la red

// Constantes
int INTERVAL = 5000;  // Determina el tiempo entre lecturas no bloqueantes

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.1.135";
IPAddress server(192,168,1,135);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

// Variables
int analogPin = A0;  // Esta variable señala el pin donde se conecta la salida analógica
int umbralPin = D1;  // Esta variable señala el pin donde se conecta la salida digital
long timeNow, timeLast;  // Control de acciones asincronas
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266

//---------Inicialización del programa, estas instrucciones se ejecutan una vez al energizar el sistema
void setup () {
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
  Serial.println ("Programa iniciado");

  // Configuración de pines
  pinMode (analogPin, INPUT);  // Lectura analógica
  pinMode (umbralPin, INPUT);  // lectura digital
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED){
  digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT

  // Inicia seguimiento de eventos temporizados no bloqueantes
  timeLast = millis ();
}// fin del void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {

  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Control de eventos temporizados no bloqueantes
  timeNow = millis();
  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo
    int dataAnalog = analogRead (analogPin);  // Realiza la lectura analogica y la guarda en una variable para poder desplegarla
    bool dataUmbral = digitalRead (umbralPin);  // Realiza la lectura digital y la guarda en una variable para poder desplegarla

    // Enviar lectura por MQTT
    char dataString [8];
    dtostrf (dataAnalog, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("esp32/flama-analog",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    dtostrf (dataUmbral, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("esp32/flama-umbral",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT

    // Mostrar los datos por seial
    Serial.print ("Lectura analogica ");
    Serial.println (dataAnalog);
    Serial.print ("Lectura digital ");
    Serial.println (dataUmbral);

  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ();

//------------- Funciones de usuario
// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
   if (String(topic) == "esp32/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "esp32/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}