int pirPin = D0; // PIR conectado a D3
int led = D7;    // LED Onboard conectado al D7

void setup()
{
    Serial.begin (9600);
  pinMode(pirPin, INPUT_PULLUP); // Inicializar D3 con la resistencia interna en HIGH
  pinMode(led, OUTPUT);          // Inicializar D7 como salida
}


void loop()
{
  int pirValState;

  pirValState = digitalRead(pirPin);

  if(pirValState == LOW){      // Se detectó movimineto?
    digitalWrite(led, HIGH);   // Prende el LED
      Particle.publish("movimiento", "1",PRIVATE); //Envia un evento "movimiento" a la nube Particle
      Serial.println ("Sended");
    delay(10000); // Espera 30 segundos para generar una nueva "imagen" del cuarto sin gente y evitar sobrecargar el servidor"
  }
  else
  {
    digitalWrite(led, LOW);  // Apaga el LED
  }
}