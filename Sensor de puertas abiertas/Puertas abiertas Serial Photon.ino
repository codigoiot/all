/*
   Sensor de puertas abiertas
   Por: Hugo Vargas
   Fecha: 26 de marzo de 2020

   Este programa manda por la nube particle la lectura del sensor de puertas abiertas
   y se visualiza en un flow en NodeRed. Este sensor no tiene polaridad, por lo que no
   pasa nada si lo conectas al revés.

   Configuración de hardware
    Sensor de puertas abiertas    Photon
    GND ------------------------- GND
    S   ------------------------- D1

*/

// Variables del prorgama
int magnetic = D1;  // Sensor de puertas abiertas conectado a D1
int led = D7;  // LED del Photon es D7

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
    // Inicialización del programa
    // Inicia comunicación serial
    Serial.begin (115200);
    while (!Serial) {}; 
    
    // Configuración de pines
    pinMode (led, OUTPUT);  // Se configura el pin del led como salida
    pinMode(magnetic, INPUT_PULLUP);  // Inicializa el Pin D1 como input con resistencia Pull-Up interna
    
    // Condiciones Iniciales
    digitalWrite (led, LOW);  // Se inicia con el led apagado
}// fin del void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
  int magneticState;  // Variable para detectar el estado del sensor

  magneticState = digitalRead(magnetic);  // Lectura del sensor

  if(magneticState == HIGH) {  // Se detecto puerta abierta??
    digitalWrite(led, HIGH);  // Prende el LED
    Serial.println ("Puerta abierta");
    delay(1000);  // Espera 1 segundo para hacer otra lectura
  }// fin del if(pirValState == HIGH)
  else  // En caso de que se detecte puerta abierta
  {
    digitalWrite(led, LOW);  // Apaga el LED
    Serial.println ("Puerta cerrada");
    delay(1000);  // Espera 1 segundo para hacer otra lectura
  }// fin del else
}// Fin del void loop()