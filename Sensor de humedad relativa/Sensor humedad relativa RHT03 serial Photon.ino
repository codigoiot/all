/*Lectura de sensor de temperatura y humedad relativa
 * Por: Hugo Vargas
 * Fecha: 23 de marzo de 2020
 * 
 * Este es el programa básico para obtener la lectura del sensor temperatura
 * y humedad relativa y observar los valores a través del monitor serial, 
 * por ejemplo, con TeraTerm.
 * 
 * Configuración de hardware
 *  Sensor de RHT               Photon
 *  - ------------------------- GND
 *  + ------------------------- 3.3v
 *  s ------------------------- D1
 *  Fotocelda------------------ A0
 * 
*/

// Constantes del programa
#define PRINT_RATE 1500  // Pausa en ms entre lectura de datos
const int RHT03_DATA_PIN = D1;  // Pin de datos del RHT03
const int LIGHT_PIN = A0;  // Salida análoga de la fotocelda
const int LED_PIN = D7;  // LED para mostrar cuando el sensor esta siendo leido

//Variables del programa
unsigned int minimumLight = 65536;  // Variable captar luz minima
unsigned int maximumLight = 0;  // Variable para captar luz maxima
float minimumTempC = 100;  // Variable para captar temperatura minima
float maximumTempC = 0;  // Variable para captar temperatura maxima
float minimumTempF = 9941;  // Variable para captar temperatura minima
float maximumTempF = 0;  // Variable para captar temperatura máxima
float minimumHumidity = 100;  // Variable para captar humedad minima
float maximumHumidity = 0;  // Variable para captar humedad maxima

//Objetos
RHT03 rht;  // Objeto para manejar el sensor

//----------Configuración del programa, esta parte se ejecuta sólo una vez al 
//----------energizarse el sistema
void setup() 
{
    // Inicialización del programa
    // Inicia comunicación serial
    Serial.begin (115200);  // Se inicia la comunicación serial a 115200 baudios
    while(!Serial){};  // Se espera a que se realice correctamente la comunicación serial
    
    // Inicialización del sensor
    rht.begin(RHT03_DATA_PIN);  // Esta instrucción manda las señales para activar el sensor
    
    //Configuración de pines
    pinMode(LIGHT_PIN, INPUT); // Pin de la fotocelda - Entrada
    pinMode(LED_PIN, OUTPUT); // Pin del LED - Salida
    
    //Condiciones Iniciales
    digitalWrite(LED_PIN, LOW); // Iniciar con el LED apagado
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() 
{
    digitalWrite(LED_PIN, HIGH);  // Se prende el led para indicar lectura
    
    // Se usa la función 'update del sensor para ller los nuevos valores de humedad y temperatura.
    // En el caso de una lectura correcta, la función devuelve un 1, en caso de que sea fallida
    // se manda un cero
    int update = rht.update();
    
    if (update == 1)  // Si la conexion con el sensor fue exitosa, imprimir los valores obtenidos
    {
        // Lectura de sensores
        
        // Lectura del sensor de luz
        unsigned int light = analogRead(LIGHT_PIN);
        // Se realiza el registro de maximos y minimos
        if (light > maximumLight) maximumLight = light;
        if (light < minimumLight) minimumLight = light;
        
        // LA función 'humidity()' regresa la última lectura
        // de humedad relativa del sensor.
        // Regresa un valor de punto flotante -- un porcentaje de RH entre 0-100.
        float humidity = rht.humidity();
        // Se realiza el registro de maximos y minimos
        if (humidity > maximumHumidity) maximumHumidity = humidity;
        if (humidity < minimumHumidity) minimumHumidity = humidity;
        
        // La función 'tempF()' regresa el valor actual de temperatura 
        // en grados farenheit del sensor.
        // Regresa una variable de punto flotante con el valor en grados Farenheit.
        float tempF = rht.tempF();
        // Se realiza el registro de maximos y minimos
        if (tempF > maximumTempF) maximumTempF = tempF;
        if (tempF < minimumTempF) minimumTempF = tempF;
        
        // `tempC()` trabaja igual que 'tempF()', pero regresa la temperatura en
        // grados Celsios.
        float tempC = rht.tempC();
        // Se realiza el registro de maximos y minimos
        if (tempC > maximumTempC) maximumTempC = tempC;
        if (tempC < minimumTempC) minimumTempC = tempC;
        
        // Imprimir valores
        
        // `Serial.print()` es usada para enviar datos desde el Photon a la computadora usando la interfase serial.
        // El parámetro que enviamos a 'print()' puede ser cualquier cosa, desde un String hasta un arreglo de caracters,
        // o un valor de punto flotante, entero, o prácticamente cualquier tipo de variable.
        Serial.print("Luz: ");
        Serial.print(light); // Imprime la lectura de iluminación
        Serial.print(" ("); // Imprime " ("
        Serial.print(minimumLight); // Imprime el valor mínimo de iluminación
        Serial.print('/'); // Print a '/' -- las comillas sencillas indican que solo enviamos un caracter
        Serial.print(maximumLight); // Imprime el valor máximo de iluminación.
        Serial.println(") (min/max)"); 
        // La línea completa se verá parecida a: "Light: 545 (8/791) (min/max)"
        
        // Imprime la temperatura en °C:
        // Ejemplo de impresión: "Temp: 24.9 °C (23.5/24.5) (min/max)"
        Serial.print("Temperatura: ");
        Serial.print(tempC, 1); // Imprime un valor de punto flotante, podemos indicar el número de decimales con el segundo parámetro 
        Serial.print(" ");
        // 'write()' se usa para enviar un valor SINGLE BYTE a través de la linea serial:
        Serial.write(248); // 248 es el valor ASCII del simbolo °.
        Serial.print("C (");
        Serial.print(minimumTempC, 1);
        Serial.print('/'); // Imprime una '/'
        Serial.print(maximumTempC, 1);
        Serial.println(") (min/max)");
        
        // Imprime la temperatura en °F:
        // Ejemplo de impresión: "Temp: 76.1 °F (74.3/76.1) (min/max)"
        Serial.print("temperatura: ");
        Serial.print(tempF, 1); // Imprime la variable tempF -- 1 decimal
        Serial.print(' ');      // Imprime un espacio
        Serial.write(248);      // Imprime el valor ASCII 248 (el simbolo °)
        Serial.print("F (");    // Imprime "F ("
        Serial.print(minimumTempF, 1); // Imprime la temperatura mínima -- 1 decimal point
        Serial.print('/');      // Imprime una '/'
        Serial.print(maximumTempF, 1); // Imprime la temperatura máxima -- 1 decimal point
        Serial.println(") (min/max)"); // Termina la línea con ") (min/max)"
        
        // Imprime la humedad relativa:
        // Ejemplo de impresión: "humedad: 29.7 % (29.10/41.80) (min/max)"
        Serial.print("Humedad: ");
        Serial.print(humidity, 1);
        Serial.print(" %");
        Serial.print(" (");
        Serial.print(minimumHumidity, 1);
        Serial.print("/");
        Serial.print(maximumHumidity, 1);
        Serial.println(") (min/max)");
        
         Serial.println(); // Imprime una linea en blanco:
    }// fin del if (update == 1)
    else // Si la comunicación con el sensor falló, se informa y se espera
    {
        Serial.println("Error reading from the RHT03."); // Imprime mensaje de error
        Serial.println(); // Imprime linea en blanco
        
        delay(RHT_READ_INTERVAL_MS); // El RHT03 necesita cerca de 1 segundo entre lecturas
    }// fin del else
    
    digitalWrite(LED_PIN, LOW);  // Una vez terminada la lectura, se apaga el led
    
    delay(PRINT_RATE);  // Se hace una pausa antes de realizar una nueva lectura
}// fin de void loop ()