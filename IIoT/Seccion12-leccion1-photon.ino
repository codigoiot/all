// This #include statement was automatically added by the Particle IDE.
#include <MFRC522.h>

// This #include statement was automatically added by the Particle IDE.
#include <MFRC522.h>

/*
-----------------------------------------------------------------------------
 * Correspondencia de pines:
 * Signal     Pin              
 *            Photon        MFRC522
 * ---------------------------------------------------------------------------
 * Reset      ANY (D2)        RST
 * SPI SS     ANY (A2)        SDA
 * SPI MOSI   A5            MOSI
 * SPI MISO   A4            MISO
 * SPI SCK    A3            SCK
 */

//Definir (desde el compilador) que pines se van a usar para SS y RESET
#define SS_PIN SS
#define RST_PIN D2
#define IN_PIN D6
#define OUT_PIN D7
int stateIn;
int stateOut;
// Crea una instancia de MFRC522
MFRC522 mfrc522(SS_PIN, RST_PIN);    


void setup() {
    Serial.begin(9600);    // Inicia comunicación serial con la PC
    mfrc522.setSPIConfig();
    mfrc522.PCD_Init();    // Inicia la palca MFRC522
    pinMode(IN_PIN,INPUT_PULLUP);
    pinMode(OUT_PIN,INPUT_PULLUP);
}

void loop() {
    // Hay etiquetas/tarjetas nuevas?
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return; //Si no continua
    }

    // Selecciona una etiqueta/tarjeta usando ReadCardSerial, si no hay una etiqueta
    // continua
    if ( ! mfrc522.PICC_ReadCardSerial()) {
        return;
    }
    //Deten la lectura hasta recibir un tag nuevo
    mfrc522.PICC_HaltA();
    //  Crea una variable vacia para almacenar el identificador único del tag (UID)
    String uid="";
    //Recorre todos los bytes de UID y agregalos al string uid.
    for (byte i = 0; i < mfrc522.uid.size; i++) {
        uid =uid+mfrc522.uid.uidByte[i];
        //Envía la uid por serial
        Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        Serial.print(mfrc522.uid.uidByte[i], HEX);
    }

    //Si uid no esta vacio...
    if(uid!=""){
        stateIn=digitalRead(IN_PIN);
        stateOut=digitalRead(OUT_PIN);
        //Si los dos botones estan apretados registrar la tarjeta
        if (stateIn==LOW && stateOut==LOW){
            //Enviar un evento "registro"
            Particle.publish("registro",uid);
            //Esperarse 3 segundos antes de volver a leer una tarjeta
            delay(3000);
        }
        else if(stateIn==LOW){ //Si "adentro" está apretado
            //Enviar un evento "adentro"
            Particle.publish("adentro",uid);
            //Esperarse 3 segundos antes de volver a leer una tarjeta
            delay(3000);
        }
        else if(stateOut==LOW){ //Si afuera está apretado
            //Enviar un evento "afuera"
            Particle.publish("afuera",uid);
            //Esperarse 3 segundos antes de volver a leer una tarjeta
            delay(3000);
        }
    }
}