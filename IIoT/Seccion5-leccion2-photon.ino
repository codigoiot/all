int val = 0; // variable para almacenar el valor de humedad
int soil = A2;// Declaramos la variable para el pin del sensor de humedad 
int soilPower = D6;//Variable for Soil moisture Power
//int soilPower = D6; // Variable para activar el sensor de humedad// En vez de alimentar el sensor a través de los pines "V-USB" o "3.3V",// vamos a usar un pin digital para activarlo. Esto evitará// la oxidación del sensor mientras esta en contacto con la corrosión del suelo. 

void setup() 
{
   Serial.begin(9600);   // abrimos la comunicación serial del USB

   pinMode(soilPower, OUTPUT); // Configuramos D6 como salida
   digitalWrite(soilPower, LOW); // Configuramos LOW en el sensor para que no reciba energía

   // Esta linea crea una variable que será publicada a través de la nube.
   // Puedes solicitar su valor usando diferentes metodos
   Particle.variable("soil", &val, INT);
}

void loop() 
{
   Serial.print("Soil Moisture = ");    
   // obtenemos el valor de humedad con la función siguiente y la imprimimos
   Serial.println(readSoil());

   delay(1000); // tomamos la lectura cada segundo
   // Este tiempo es usado para que puedas probar el sensor y verlo cambiar en tiempo real.
   // Para una aplicación real usaríamos una lectura mucho menos frecuente.

    // Si el suelo esta muy seco, encendemos el LED con color rojo para notificarnos
    // Este valor dependerá de tu terreno y tus plantas
    if(readSoil() < 200)
    {
      // tomamos control del LED RGB
      RGB.control(true);
      RGB.color(255, 0, 0); // ponemos en rojo el LED RGB
    }
    else
    {
      // regresamos a la operación normal
      RGB.control(false);
    }
}
// Esta función es usada para obtener el valor de humedad del suelo
int readSoil()
{
    digitalWrite(soilPower, HIGH); // prendemos D6 ("On")
    delay(10);//wait 10 milliseconds 
    val = analogRead(soil); // leemos la humedad
    digitalWrite(soilPower, LOW); // apagamos D6 ("Off")
    return val;
}