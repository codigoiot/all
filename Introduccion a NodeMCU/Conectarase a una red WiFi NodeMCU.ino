/*
 * Conexión simple a WiFi
 * Por: Hugo Escalpelo
 * Fecha: 25 de febrero de 2020
 * 
 * Este programa muestra una conexión básica a WiFi. Para ello
 * es necesario que cambies el SSID y contraseña por los datos
 * correspondientes a la red que deseas conectarte. Puedes 
 * observar el estado de la conexión con el monitor serial.
 * No se requieren componentes adicionales
 */

// Bibliotecas
#include <ESP8266WiFi.h>  // Esta biblioteca se agrega automaticamente
                          // al configurar el nodeMCU

// Datos de Red
const char* ssid = "nombre_de_red";  // Pon aquí el nombre de la red a la que deseas conectarte
const char* password = "11223344";  // Escribe la contraseña de dicha red

// Objetos
WiFiClient espClient; // Este objeto maneja las variables necesarias para una conexion WiFi

// Variables del programa
int ledPin = D4;  // Esta permite controlar el led en la placa para indicarnos que hay conexión
double timeLast, timeNow; // Variables para el control de tiempo no bloqueante
double wait = 5000;  // Espera de 2 minutos para consultar conexión

// Inicialización del programa
void setup() {
  //Configuración de pines
  pinMode (ledPin, OUTPUT);
  digitalWrite (ledPin, LOW);

  //Inicialización de comunicación serial
  Serial.begin (115200);
  Serial.println();
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password); // Esta es la función que realiz la conexión a WiFi
 
  while (WiFi.status() != WL_CONNECTED) { // Este bucle espera a que se realice la conexión
    digitalWrite (ledPin, HIGH);
    delay(500); //dado que es de suma importancia esperar a la conexión, debe usarse espera bloqueante
    digitalWrite (ledPin, LOW);
    Serial.print(".");  // Indicador de progreso
    delay (5);
  }

  // Cuando se haya logrado la conexión, el programa avanzará, por lo tanto, puede informarse lo siguiente
  Serial.println();
  Serial.println("WiFi conectado");
  Serial.println("Direccion IP: ");
  Serial.println(WiFi.localIP());

  // Si se logro la conexión, encender led
  if (WiFi.status () > 0){
  digitalWrite (ledPin, HIGH);
  }
  timeLast = millis (); // Inicia el control de tiempo
}

//Cuerpo del programa, bucle principal
void loop() {
  timeNow = millis ();  // Seguimiento de tiempo
  if (timeNow - timeLast > wait) {  // Comparación de tiempo no bloqueante cada 2 minutos
    if (WiFi.status ()) {  //Comprobar que haya conexión
      digitalWrite (ledPin, LOW);  //En caso de haber conexión, encender el led
    } else {
      digitalWrite (ledPin, HIGH); //Si no hay conexión, apagar el led
    }
  }
  delay (1000);
}