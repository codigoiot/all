const int speakerPin = D2; // zumbador conectado a D2

// El numero de notas y silencios de la canción
const int songLength = 18;

// notes es un arreglo de caracteres que corresponde a las notas
// de la cancion. Los espacios son silencios (noTone).
// Para cambiar la cancion cambia las notas de este arreglo
char notes[] = "cdfda ag cdfdg gf "; // a space represents a rest

// beats es un arreglo de valores para la duracion de cada nota.
// Un "1" representa u cuarto de tiempo, 2 un medio, etc.
// No olvides que los silencios tambien duran.
int beats[] = {1,1,1,1,1,1,4,4,2,1,1,1,1,1,1,4,4,2};

// El tempo es que tan rápido se tocara la canción.
// Para hacerla mas rápida reduce este valor.
int tempo = 150;


void setup() 
{
  pinMode(speakerPin, OUTPUT);

  // Solo quieres que se toque la cancion una vez así que la ponemos en el setup
  int i, duration;

  for (i = 0; i < songLength; i++) // recorre los arreglos notes y beat
  {
    duration = beats[i] * tempo;  // duración de la nota en ms

    if (notes[i] == ' ')          // ¿toca un silencio?
    {
      delay(duration);            // entonces espera un momento
    }
    else                          // si no toca la nota
    {
      tone(speakerPin, frequency(notes[i]), duration);
      delay(duration);            // espera a que la nota termine
    }
    delay(tempo/10);              // y pausa entre notas
  }

  //Si quieres que tu canción se repita pégala en el loop() 
}


void loop() 
{
//no hace nada
}


int frequency(char note) 
{
  // Esta función toma el carácter de una nota (a-g) y regresa
  // La frecuencia que le corresponde ne Hz para que la use tone()
  int i;
  const int numNotes = 8;  // cuantas notas existen

  // Los siguientes arreglos contienen las notas y sus
  // frecuencias correspondientes. La ultima "C" es mayúscula
  // para separarla de la primera "c". Si quieres agregar
  // notas cada una debe de tener un carácter único

  // Los valores de tipo char van en comillas sencillas ''
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int frequencies[] = {262, 294, 330, 349, 392, 440, 494, 523};

  // El siguiente ciclo busca la nota que queremos y cuando
  // la encuentra regresa su frecuencia.

  for (i = 0; i < numNotes; i++)  // Itera sobre las notas
  {
    if (names[i] == note)         // Esta es la que estas buscando?
    {
      return(frequencies[i]);     // Si sí regresa la frecuencia
    }
  }
  return(0);  // Si no encuentra la nota regresa 0
}