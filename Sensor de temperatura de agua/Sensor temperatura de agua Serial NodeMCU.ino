// Incluir las bibliotecas necesarias//La instalación de Arduino-Temperature-Control-Library agrega ambas, onewire.h y dallastemperature.h#include <onewire.h>#include <dallastemperature.h>

// Indicar en qué pin se conectará el sensor de temperatura
#define ONE_WIRE_BUS 2

// Iniciar instancia de OneWire, la cual manejará el pin de comunicación
OneWire oneWire(ONE_WIRE_BUS);

// Pasar la referencia del pin a la biblioteca DallasTemperature, la encargada de manejar el sensor 
DallasTemperature sensors(&oneWire);

// Inicialización del programa, las instrucciones que se ejecutan solo una vez al energizar
void setup(void)
{
  // Iniciar comunicación serial
  Serial.begin(9600);
  Serial.println("Dallas Temperature IC Control Library Demo");

  // Iniciar la biblioteca de comunicación
  sensors.begin();
}

//Cuerpo del programa, instrucciones que se repiten ciclicamente
void loop(void)
{ 
  // Llamar sensors.requestTemperatures() para obtener una lectura de todos los sensores disponibles 
  Serial.print("Solicitando temperaturas...");
  sensors.requestTemperatures(); // Enviar el comando que solicita las temperaturas
  Serial.println("Listo!");
  // Temperaturas obtenidas
  // En caso de haber varios sensores, se colocan en un arreglo,  // para este ejemplo se visualiza el valor del primer sensor
  Serial.print("La temperatura del dispositivo 1 (indice 0) es: ");
  Serial.println(sensors.getTempCByIndex(0));  
}