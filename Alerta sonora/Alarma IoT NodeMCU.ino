/* Detonar una alarma
   Por: Hugo Vargas
   Fecha: 24 de marzo de 2020

   Este es el prorgama básico de IOT con NodeMCU CP2102, MQTT y Node-Red.
   Este programa realiza simula un sensor digital con un boton y un sensor
   analogico con un slider. Manda los datos por MQTT y el NodeMCU los recupera
   La alarma se activa al cumplise las condiciones necesarias.

   Configuración de hardware
    Buzzer                      NodeMCU
    - ------------------------- GND
    + ------------------------- D2

*/

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "AXTEL XTREMO-18D6";  // Nombre de red
const char* password = "038C18D6";  // Contraseña de la red

// Constantes del programa
const int INTERVAL = 1000;  //Tiempo en milisegundos entre lecturas
const int speakerPin = D2; // zumbador conectado a D2
const int songLength = 18;  // El numero de notas y silencios de la cancion

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.15.3";
IPAddress server(192, 168, 15, 3);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
long timeNow, timeLast;  // Control de acciones asincronas
char notes[] = "cdfda ag cdfdg gf ";  // Arreglo de notas
int beats[] = {1, 1, 1, 1, 1, 1, 4, 4, 2, 1, 1, 1, 1, 1, 1, 4, 4, 2}; // Duracion de las notas
int tempo = 150;  // Velocidad de las notas
int threshold = 2000;  // valor de comparación para reproducir la alarma


//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  //Configuración de pines
  pinMode(speakerPin, OUTPUT);  // Buzzer como salida

  //Condiciones Iniciales
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);
  digitalWrite(speakerPin, LOW);  // Se inicia con el buzzer apagado

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED) {
    digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT

  // Inicio de control de eventos asincronos
  timeLast = millis ();
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Funciones temporizadas no bloqueantes
  timeNow = millis();

  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo


    //Serial.print("Humedad de suelo = ");  // Se envía un mensaje que informa de la lectura
    //Serial.println(readSoil());  // Se manda a llamar la función que se encarga de leer el sensor

    // Enviar lectura por MQTT
    //char dataString [8];
    //dtostrf (val, 1, 2, dataString);  // Convierte a String la lectura del sensor
    //client.publish ("esp32/hum",dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    //Serial.print ("Lectura de humedad: ");  // Se reporta también por serial
    //Serial.println (val);
  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ()

//------------- Funciones de usuario
// Funcion que reproduce la tonada
void reproducir () {
  int i, duration;

  for (i = 0; i < songLength; i++)  // recorre los arreglos notes y beat
  {
    duration = beats[i] * tempo;  // duracion de la nota en ms
    if (notes[i] == ' ')  // ¿toca un silencio?
    {
      delay(duration);  // entonces espera un momento
    }// fin del if (notes[i] == ' ')
    else  // si no toca la nota
    {
      tone(speakerPin, frequency(notes[i]), duration);  // Se reproduce la tonada
      delay(duration);  // espera a que la nota termine
    }// fin del else
    delay(tempo / 10); // y pausa entre notas
  }// fin del for (i = 0; i < songLength; i++)
}// fin del void reproducir ()

// Funcion que calcula las frecuencias de la tonada
int frequency(char note)
{
  // Esta funcion toma el caracter de una nota (a-g) y regresa
  // La frecuencia que le corresponde ne Hz para que la use tone()
  int i;
  const int numNotes = 8;  // cuantas notas existen

  // Los siguientes arreglos contienen las notas y sus
  // frecuencias correspondientes. La ultima "C" es mayuscula
  // para separarla de la primera "c". Si quieres agregar
  // notas cada una debe de tener un caracter unico

  // Los valores de tipo char van en comillas sencillas ''
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int frequencies[] = {262, 294, 330, 349, 392, 440, 494, 523};

  // El siguiente ciclo busca la nota que queremos y cuando
  // la encuentra regresa su frecuencia.

  for (i = 0; i < numNotes; i++)  // Itera sobre las notas
  {
    if (names[i] == note)         // Esta es la que estas buscando?
    {
      return (frequencies[i]);    // Si sí regresa la frecuencia
    }
  }
  return (0); // Si no encuentra la nota regresa 0
}// fin del int frequency(char note)

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false

  if (String(topic) == "codigoiot/output") {  // Si se recibe el mensaje true, se activa la alarma
    Serial.print("Se recibio ");
    if (messageTemp == "true") {
      Serial.println("digital");
      reproducir ();
    }// fin de if(messageTemp == "true")
    else { // De no recibirse el mensaje true, se compara con un valor analógico
      Serial.println("analogico");
      Serial.println (messageTemp.toInt());
      if (messageTemp.toInt() > threshold) {
        Serial.println ("Este valor activa la alarma");
        reproducir ();
      } else {
        Serial.println ("Valor aceptable");
      }
    }// fin de else if(messageTemp == "false")
  }// fin de if (String(topic) == "codigoiot/output")

}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("codigoiot/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}