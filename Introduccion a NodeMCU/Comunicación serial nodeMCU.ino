/*
 * Comunicación serial para nodeMCU
 * por: Hugo Vargas
 * Fecha: 25 de febrero de 2020
 * 
 * Este programa muestra la comunicación serial entre el nodeMCU y la PC. Para ello,
 * se crea una comunicación serial a 115200 baudios y el micro controlador responde
 * con un contador.
 */

// Variables del programa
double timeLast, timeNow;   // Variables para control de tiempo no bloqueante
int counter = 0;    //Contador
int wait = 1000;    //Espera entre envíos

// Inicialización del programa
void setup() {
  // Iniciar comunicación serial
  Serial.begin (115200);    // Esta función inicia la comunicación
  Serial.println ("Comunicación iniciada");   // Se envía mensaje de prueba

  // Control de tiempo no bloqueante
  timeLast = millis ();
}// fin del void setup 

// Cuerpo del programa, bucle principal
void loop() {
  timeNow = millis ();    // Control de tiempo
  if (timeNow - timeLast > wait){   //Esta comparación verifica que haya pasado 1 segundo
    timeLast = timeNow;   //actualiza el valor del control de tiempo  
    counter++;    // Incrementa el valor del contador
    Serial.print ("Contador: ");    // reporta el valor del contador
    Serial.println (counter);   // Imprime el valor del contador
  }
}