/*Lectura de sensor de temperatura y humedad relativa
 * Por: Hugo Vargas
 * Fecha: 23 de marzo de 2020
 * 
 * Este es el programa básico para obtener la lectura del sensor temperatura
 * y humedad relativa y observar los valores a través del monitor serial, 
 * por ejemplo, con TeraTerm.
 * 
 * Configuración de hardware
 *  Sensor de RHT               Photon
 *  - ------------------------- GND
 *  + ------------------------- 3.3v
 *  s ------------------------- D2
 *  Fotocelda------------------ A0
 * 
*/

// Bibliotecas
#include "DHT.h"

// Constantes del programa
#define DHTPIN 2  // Pin donde se conecta el sensor digital
const int LIGHT_PIN = A0;  // Salida análoga de la fotocelda
#define WAIT 1500  // Espera entre lecturas
// Puedes seleccionar distintos sensores para trabajar,
// descomenta el que vayas a usar y comenta los que no
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Objetos
DHT dht(DHTPIN, DHTTYPE);

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);  
  while(!Serial){};

  //Configuración de pines
  pinMode(LIGHT_PIN, INPUT); // Pin de la fotocelda - Entrada

  dht.begin();  // Inicia la comunicación digital con el sensor
}// Fin del void setup()

  //----------Cuerpo del programa, bucle principal
void loop() {

  // Lectura del sensor de luz
  unsigned int light = analogRead(LIGHT_PIN);  // Lee el sensor de luz

  //  Lectura del sensor
  float h = dht.readHumidity();  // Lee la humedad
  float t = dht.readTemperature();  // Lee la temperatura en centigrados
  float f = dht.readTemperature(true);  // Lee la temperatura en farenheit

  // Comprueba que se hayan realizado las lecturas, en caso contrario,
  // intenta de nuevo
  if (isnan(h) || isnan(t) || isnan(f)) {  // Si algun valor está vacío
    Serial.println(F("Falló la lectura del sensor"));
    return;
  }// fin del if (isnan(h) || isnan(t) || isnan(f))

  // Imprime los valores de las lecturas
  Serial.print (F("Luz "));
  Serial.print (light);
  Serial.print(F(" Humedad: "));
  Serial.print(h);
  Serial.print(F("%  Temperatura: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.println(F("°F"));
  
}//Fin del void loop()