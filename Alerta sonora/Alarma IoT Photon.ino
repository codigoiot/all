/*
Detonar una alarma
Por: Hugo Vargas
Fecha: 24 de marzo de 2020

Estre programa toma datos de la nube para detonar una alarma. Estos datos
simulan un sensor digital que simplemente manda un valor logico verdadero
al activarse y un sensor analógico, el cual manda el valor de su medición.
Los sensores están simulados en NodeRed y este programa realiza las 
comparaciones necesarias para detonar la alarma al detectar las condiciones
necesarias.

Configuración de hardware
Buzzer                      Photon
- ------------------------- GND
+ ------------------------- D2
 
*/


// Constantes del programa
const int speakerPin = D2; // zumbador conectado a D2
const int songLength = 18;  // El numero de notas y silencios de la cancion

//Variables del programa
char notes[] = "cdfda ag cdfdg gf ";  // Arreglo de notas
int beats[] = {1,1,1,1,1,1,4,4,2,1,1,1,1,1,1,4,4,2};  // Duracion de las notas
int tempo = 150;  // Velocidad de las notas
int threshold = 2000;  // valor de comparación para reproducir la alarma

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
    // Inicialización del programa
    // Inicia comunicación serial
    Serial.begin (115200);  
    while(!Serial){};
    
    // Configuración de pines
    pinMode(speakerPin, OUTPUT);  // Buzzer como salida
    
    // Condiciones Iniciales
    digitalWrite(speakerPin, LOW);  // Se inicia con el buzzer apagado
    
    // Funciones Párticle
    Particle.function("alerta", funcionAlerta);  // Para detectar sensores digitales
    Particle.subscribe("nivel", funcionAlerta2, MY_DEVICES);  // Para monitorear sensores analógicos
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop()
{
   // No hacemos nada, este programa solo espera la llamada para activar la función
   // Se simulará la lectura de algun sensor desde NodeRed
}// Fin del void loop()

//------------- Funciones de usuario

// Funcion que reproduce la tonada
void reproducir () {
  int i, duration;

  for (i = 0; i < songLength; i++)  // recorre los arreglos notes y beat
  {
    duration = beats[i] * tempo;  // duracion de la nota en ms
    if (notes[i] == ' ')  // ¿toca un silencio?
    {
      delay(duration);  // entonces espera un momento
    }// fin del if (notes[i] == ' ')
    else  // si no toca la nota
    {
      tone(speakerPin, frequency(notes[i]), duration);  // Se reproduce la tonada
      delay(duration);  // espera a que la nota termine
    }// fin del else
    delay(tempo/10);  // y pausa entre notas
  }// fin del for (i = 0; i < songLength; i++)
}// fin del void reproducir ()

// Funcion que calcula las frecuencias de la tonada
int frequency(char note) 
{
  // Esta funcion toma el caracter de una nota (a-g) y regresa
  // La frecuencia que le corresponde ne Hz para que la use tone()
  int i;
  const int numNotes = 8;  // cuantas notas existen

  // Los siguientes arreglos contienen las notas y sus
  // frecuencias correspondientes. La ultima "C" es mayuscula
  // para separarla de la primera "c". Si quieres agregar
  // notas cada una debe de tener un caracter unico

  // Los valores de tipo char van en comillas sencillas ''
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int frequencies[] = {262, 294, 330, 349, 392, 440, 494, 523};

  // El siguiente ciclo busca la nota que queremos y cuando
  // la encuentra regresa su frecuencia.

  for (i = 0; i < numNotes; i++)  // Itera sobre las notas
  {
    if (names[i] == note)         // Esta es la que estas buscando?
    {
      return(frequencies[i]);     // Si sí regresa la frecuencia
    }
  }
  return(0);  // Si no encuentra la nota regresa 0
}// fin del int frequency(char note)

//Funcion que llama la reproducción por sensor digital
int funcionAlerta(String command) {
    
    if (command=="on") {
        Serial.println ("Reproduciendo alarma");
        reproducir ();  // La tonada solo se reproduce al recibir el mensaje on
        return 1;
    }
    else {  // En caso contrario, manda un código de error
        return -1;
    }
}// fin del int funcionAlerta(String command)

// Funcion que llama la reproducción por sensor analógico
void funcionAlerta2(const char *event, const char *data)
{
    int nData = String(data).toInt();  //Converison de char a string
  if (nData > threshold) {  // Si se supera el nivel predeterminado
    Serial.println ("Este valor detona la alarma");
    reproducir ();  // Reproducir alarma
  }
  else {
    Serial.println ("Valor aceptable");
  }
}