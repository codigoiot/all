/*
 * Hello World para nodeMCU
 * Por: Hugo Vargas
 * Fecha: 25 de febrero de 2020
 * 
 * Este programa es el más básico para demostrar el uso y carga de programas 
 * al nodeMCU con lenguaje Arduino. Este programa usa el led que está soldado
 * en la placa y el led soldado en el modulo ESP8266 y hace que parpadee. 
 * No se requiere hardware extra.
 */

// Variables del programa
int ledPin = D0;                   // Se declara la variable ledPin, la cual controla el led de la tarjeta
int ledPin2 = D4;                  // Controla el pin sobre el modulo ESP8266
int wait = 500;                    // Esta variable determina cada cuanto tiempo se hará parpadear el led
bool ledState = 0;                 // Esta variable lleva el control del estado del led
double timeNow, timeLast;          // Variables para el control de tiempo, esto permite hacer que el led
                                   // sea controlado sin bloquear el programa

// Inicialización del programa
void setup() {
  // Configuración de pines
  pinMode (ledPin, OUTPUT);         // Se declara el pin del led como salida de voltaje
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, LOW);       // Se establecen las condiciones iniciales del pin del led, comenzará apagado
  digitalWrite (ledPin2, LOW);

  // Inicio del control de tiempo
  timeLast = millis ();             // La función millis () devuelve la cantidad de mili segundos que han pasado
                                    // desde el arranque del micro controlador
}// fin de void setup ()

// Cuerpo del programa, bucle principal
void loop() {
  timeNow = millis ();              // Actualiza el seguimiento de tiempo
  if (timeNow - timeLast > wait){   // Esta comparación permite saber si han transcurrido 500 ms
    timeLast = timeNow;             // Se actualiza el control de tiempo

    // Cambiar el estado del led
    ledState = !ledState;
    digitalWrite (ledPin, ledState);// Se escribe el valor actual del led soldado en la placa
    digitalWrite (ledPin2, !ledState);// Se escribe el valor complementario en el led del modulo ESP
  }// fin de la comparación de tiempo
}// fin del void loop ()