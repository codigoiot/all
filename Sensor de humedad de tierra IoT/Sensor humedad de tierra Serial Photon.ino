/*Lectura de sensor de humedad de tierra
 * Por: Hugo Vargas
 * Fecha: 23 de marzo de 2020
 * 
 * Este es el programa básico para obtener la lectura del sensor de humedad
 * de tierra y observar los valores a través del monitor serial, por ejemplo,
 * con TeraTerm.
 * 
 * Configuración de hardware
 *  Sensor de humedad           Photon
 *  - ------------------------- GND
 *  + ------------------------- D7
 *  s ------------------------- A0
 * 
*/

//Variables del programa
int val = 0; // variable para almacenar el valor de la lectura
int soil = A0; // Variable para el pin del sensor de humedad 
int soilPower = D7; // Variable para activar el sensor de humedad
// En vez de alimentar el sensor a través de los pines "V-USB" o "3.3V", 
// vamos a usar un pin digital para activarlo. Esto evitará 
// la oxidación del sensor mientras esta en contacto con la corrosión del suelo. 

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() 
{
    // Inicialización del programa
    // INicia comunicación serial
    Serial.begin (115200);  // Se inicia la comunicación serial a 115200 baudios
    while(!Serial){};  // Se espera a que se realice correctamente la comunicación serial
    
    //Configuración de pines
    pinMode(soilPower, OUTPUT); // Configuramos D7 como salida
    
    //Condiciones Iniciales
    digitalWrite(soilPower, LOW);  // Se inicia con el sensor apagado
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() 
{
    Serial.print("Humedad de suelo = ");  // Se envía un mensaje que informa de la lectura
    Serial.println(readSoil());  // Se manda a llamar la función que se encarga de leer el sensor

    delay(1000);  // tomamos la lectura cada segundo
     // Este tiempo es usado para que puedas probar el sensor y verlo cambiar en tiempo real.
     // Para una aplicación real usaríamos una lectura mucho menos frecuente.
}// fin de void loop ()

//------------- Funciones de usuario

// Esta función es usada para obtener el valor de humedad del suelo
int readSoil()  // Es de tipo int porque tiene como retorno un numero entero
{
    digitalWrite(soilPower, HIGH);  // Se activa el pin que alimenta el sensor
    delay(10);  // Se esperan 10ms para que el sensor se enrgice correctamente
    val = analogRead(soil);  // Se lee la humedad
    digitalWrite(soilPower, LOW); // Se apaga el sensor
    return val;  // Se retorna el valor de la lectura
}