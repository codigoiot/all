// Incluir las bibliotecas necesarias
#include <Wire.h>
#include <Adafruit_MLX90614.h>

//Generar instancia de objeto que manejará las variables y funciones de la lectura del sensor
Adafruit_MLX90614 mlx = Adafruit_MLX90614();

//Inicialización de programa, la parte del programa que sucede sólo una vez al energizar
void setup() {

  //Iniciar comunicación serial
  Serial.begin(9600);
  Serial.println("Adafruit MLX90614 test");  
  
  //Inicio de comunicación con el sensor
  mlx.begin();  
}

//Cuerpo del programa, la parte que se repite constantemente
void loop() {

  //Secuencia donde se imprime la lectura del sensor mlx.readAmbientTempC ()
  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC()); 
  Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempF()); 
  Serial.print("*F\tObject = "); Serial.print(mlx.readObjectTempF()); Serial.println("*F");

  Serial.println();

  //Espera entre lecturas
  delay(500);
}