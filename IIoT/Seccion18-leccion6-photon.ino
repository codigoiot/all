// This #include statement was automatically added by the Particle IDE.
#include <MQTT.h>

//Definimos el pin D7 como control del rele
#define rele D7
#define  connectPin D2

//////////////////
// CLIENTE MQTT //
/////////////////
//Direccion IP del servidor (CAMBIAR POR LA DEL GATEWAY)
byte server[] = { 192,168,137,1};
/*Creación de un objeto MQTT con la dirección del servidor,
el puerto y la función callback que se debe llamar cada vez
que se reciba un mensaje*/
MQTT client(server, 1883, releCallback);
char clientId[]="miNombre"; //CAMBIAR POR EL NOMBRE QUE SEA
char tema[]="seccion18/rele"; //miCLiente DEBE DE SER IGUAL QUE ARRIBA

void releCallback(char* topic, byte* payload, unsigned int length) {
    //Convertir el payload en un String llamado message
    char p[length + 1];
    memcpy(p, payload, length); 
    p[length] = NULL;
    String message(p); 
    
    //Si el mensaje es "ON" prende el rele
    if(message=="ON"){
        digitalWrite(rele,HIGH);
    //Si es "OFF" apagalo
    }else if (message=="OFF"){
        digitalWrite(rele,LOW);
    }
}

/////////////////////////
// CONEXIÓN A INTERNET //
/////////////////////////

/*
* Usar modo semi-automático para que el Photon se conecte
* a wi-fi pero no a la nube Particle. Esto hace que la
* comunicación sea sólo entre el equipo y
* el Photon.
*/
SYSTEM_MODE(SEMI_AUTOMATIC);

void setup() {
    //Conectar a wi-fi
    WiFi.connect();
    waitFor(WiFi.ready,15000);
    
    //Modo de los pines
    pinMode(connectPin,INPUT_PULLUP);
    pinMode(rele,OUTPUT);
    
    //Conexión y subscripción MQTT
    client.connect(clientId);
    if (client.isConnected()) {
        client.subscribe(tema);
    }
}

void loop() {
    //Si el cliente está conectado: actualizarlo
    if (client.isConnected()) {
        client.loop();
    }
    //Si no lo está: conectarlo
    else {
        client.disconnect(); // cerrar la conexión
        client.connect(clientId);
        client.subscribe(tema);
    }
    
    //Si se presiona el botón conectar a la nube Particle
    if (digitalRead(connectPin)==0){
        Particle.connect();
        waitFor(Particle.connected,30000);
    }
}