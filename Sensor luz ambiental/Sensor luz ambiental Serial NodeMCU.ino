//Declarando variables

int LEDPIN = 13;         //Pin para controlar el led integrado
int LIGHTSENSORPIN = A0; //Pin de lectura analógica para leer el sensor

//Inicialización del programa
void setup() {

  //Configuración de pines
  pinMode(LIGHTSENSORPIN,  INPUT);  
  pinMode(LEDPIN, OUTPUT); 

  //Inicialización de comunicación serial
  Serial.begin(9600);
}

//Cuerpo del programa
void loop() {
  float reading = analogRead(LIGHTSENSORPIN); //Leer el nivel de luz
  float square_ratio = reading / 1023.0;      //Obtener el porcentaje de luz
  square_ratio = pow(square_ratio, 2.0);      //Ajuste de lectura

  analogWrite(LEDPIN, 255.0 * square_ratio);  //Ajustar el brillo del led de forma relativa
  Serial.println(reading);                    //Mostrar la lectura en el monitor serial
}