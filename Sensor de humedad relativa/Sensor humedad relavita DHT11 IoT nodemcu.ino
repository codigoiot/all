/*Lectura de sensor de temperatura y humedad relativa
   Por: Hugo Vargas
   Fecha: 23 de marzo de 2020

   Este es el prorgama básico de IOT con NodeMCU CP2102, MQTT y Node-Red.
   Este programa realiza una lectura digital del pin donde está conectado
   el sensor de temperatura y humedad relativ, y una lectura analogica en
   el pin donde esta conectado el sensor de luz. Luego se reportan los
   valores obtenidos por Internet a través de MQTT a un servidor en
   Node-Red que se encarga de desplegar un tablero con la lectura.

   Configuración de hardware
    Sensor de RHT               Photon
    - ------------------------- GND
    + ------------------------- 3.3v
    s ------------------------- D2
    Fotocelda------------------ A0

*/

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT
#include "DHT.h"  // Biblioteca para leer el sensor DHT22

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "AXTEL XTREMO-1CE9";  // Nombre de red
const char* password = "037E1CE9";  // Contraseña de la red

// Constantes del programa
const int INTERVAL = 1000;  //Tiempo en milisegundos entre lecturas
#define DHTPIN 2  // Pin donde se conecta el sensor digital
const int LIGHT_PIN = A0;  // Salida análoga de la fotocelda
// Puedes seleccionar distintos sensores para trabajar,
// descomenta el que vayas a usar y comenta los que no
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.15.3";
IPAddress server(192, 168, 15, 3);

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT
DHT dht(DHTPIN, DHTTYPE);  // Sensor DHT

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266
long timeNow, timeLast;  // Control de acciones asincronas
unsigned int light;  // Para almacenar el valor de luz
float h;  // Para almacenar el valor de la humedad
float t;  // Para almacenar el valor de la temperatura en °C
float f;  // Para almacenar el valor de la temperatura en °F


//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Configuración de pines
  pinMode(LIGHT_PIN, INPUT); // Pin de la fotocelda - Entrada

  // Inicialización de sensores
  dht.begin();  // Inicia la comunicación digital con el sensor

  // Condiciones Iniciales
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED) {
    digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT

  // Inicio de control de eventos asincronos
  timeLast = millis ();
}// Fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Funciones temporizadas no bloqueantes
  timeNow = millis();

  if (timeNow - timeLast > INTERVAL) {

    // Lectura del sensor de luz
    light = analogRead(LIGHT_PIN);  // Lee el sensor de luz

    //  Lectura del sensor
    h = dht.readHumidity();  // Lee la humedad
    t = dht.readTemperature();  // Lee la temperatura en centigrados
    f = dht.readTemperature(true);  // Lee la temperatura en farenheit

    // Comprueba que se hayan realizado las lecturas, en caso contrario,
    // intenta de nuevo
    if (isnan(h) || isnan(t) || isnan(f)) {  // Si algun valor está vacío
      Serial.println(F("Falló la lectura del sensor"));
      return;
    }// fin del if (isnan(h) || isnan(t) || isnan(f))

    // Enviar lectura por MQTT
    char dataString [8];
    dtostrf (light, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("codigoiot/luz", dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("Luz: ");  // Se reporta también por serial
    Serial.println (light);
    
    dtostrf (h, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("codigoiot/hum", dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("Humedad: ");  // Se reporta también por serial
    Serial.println (h);

    dtostrf (t, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("codigoiot/tempC", dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("TempC: ");  // Se reporta también por serial
    Serial.println (t);

    dtostrf (f, 1, 2, dataString);  // Convierte a String la lectura del sensor
    client.publish ("codigoiot/tempF", dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
    Serial.print ("TempF: ");  // Se reporta también por serial
    Serial.println (t);

    timeLast = timeNow;  // Actualiza seguimiento de tiempo
  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ()

//------------- Funciones de usuario

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
    if (String(topic) == "esp32/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
    }// fin de if (String(topic) == "esp32/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}