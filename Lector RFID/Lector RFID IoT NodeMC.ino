/*
   Lector de tarjetas RFID
   Por: Hugo Vargas
   Fecha: 1 de junio de 2020

   Este programa manda por serial la lecuta tarjeta RFID por MQTT a un
   panel web en NodeRed

   Configuración de hardware
    Lector RFID                     NodeMCU
    3.3v  ------------------------- 3.3v
    RST   ------------------------- D2
    GND   ------------------------- GND
    MISO  ------------------------- MISO/D6
    MOSI  ------------------------- MOSI/D7
    SCk   ------------------------- CLK D5
    SDA   ------------------------- CS/D8

*/

// Bibliotecas
#include <ESP8266WiFi.h>  // Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h>  // Biblioteca MQTT
#include <MFRC522.h>  // Bilbioteca del lector de tarjetas RFID

// Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "AXTEL XTREMO-18D6";  // Nombre de red
const char* password = "038C18D6";  // Contraseña de la red

// Dirección de Broker MQTT
const char* mqtt_server = "192.168.15.3";
IPAddress server(192, 168, 15, 3);

// Constantes del programa
#define SS_PIN SS //Definir (desde el compilador) que pin se van a usar para SS
#define RST_PIN D2 //Definir (desde el compilador) que pin se van a usar para RESET

//Objetos
WiFiClient espClient;  // Cliente WiFi
PubSubClient client(espClient);  // Cliente MQTT
MFRC522 mfrc522(SS_PIN, RST_PIN); // Crea una instancia de MFRC522

//Variables del programa
int ledPin = D0;  // Controla el led montado en el nodeMCU
int ledPin2 = D4;  // Controla el led montado en el ESP8266

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
  // Inicialización del programa
  // INicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Configuración de pines
  pinMode (ledPin, OUTPUT);  // Se configuran los pines de los LEDs indicadores como salida
  pinMode (ledPin2, OUTPUT);
  

  // Condiciones Iniciales
  digitalWrite (ledPin, HIGH);  // Se inicia con los LEDs indicadores apagados
  digitalWrite (ledPin2, HIGH);

  // Inicializaciones
  mfrc522.PCD_Init();    // Inicia la palca MFRC522
  
  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  // Esta es la función que realiz la conexión a WiFi
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);  // Se hace parpadear el led mientras se logra la conexión
    delay(250);
    Serial.print(".");  // Se da retroalimentación al puerto serial mientras se logra la conexión
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }// fin de while (WiFi.status() != WL_CONNECTED)
  Serial.println();
  Serial.println("WiFi conectado");  // Una vez lograda la conexión, se reporta al puerto serial
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED) {
    digitalWrite (ledPin2, LOW);  // Una vez lograda la conexión se encienede el led sobre el ESP8266
  }// fin de if (WL_CONNECTED)
  delay (1000);

  // Conexion al broker MQTT
  client.setServer(server, 1883);  // Se hace la conexión al servidor MQTT
  client.setCallback(callback);  //Se activa la función que permite recibir mensajes de respuesta
  delay(1500);
  digitalWrite (ledPin, LOW);  // Se activa el led en el nodeMCU para indicar que hay conexión MQTT
}// Fin del void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  // Loop de cliente MQTT

  // Lectura RFID
  if ( ! mfrc522.PICC_IsNewCardPresent()) {  // Hay etiquetas/tarjetas nuevas?
        return;  //Si no continua
    }// Fin del if ( ! mfrc522.PICC_IsNewCardPresent())

    //  Selecciona una etiqueta/tarjeta usando ReadCardSerial, si no hay una etiqueta
    //  continua
    if ( ! mfrc522.PICC_ReadCardSerial()) {
        return;
    }// fin del if ( ! mfrc522.PICC_ReadCardSerial())
    
    mfrc522.PICC_HaltA();  //Deten la lectura hasta recibir un tag nuevo
    
    String uid="";  //  Crea una variable vacia para almacenar el identificador único del tag (UID)
    
    for (byte i = 0; i < mfrc522.uid.size; i++) {  // Recorre todos los bytes de UID y agregalos al string uid.
        uid =uid+mfrc522.uid.uidByte[i];
        //Envía la uid por serial
        Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        Serial.print(mfrc522.uid.uidByte[i], HEX);
    }

    Serial.println();
  // Enviar lectura por MQTT
  char dataString [15];
  uid.toCharArray(dataString,16); // Convierte a charr array la lectura del sensor
  client.publish ("codigoiot/rfid", dataString); // Enviar el valor de la lectura al tema adecuado en MQTT
  delay(1000);  // Espera 1 segundo para hacer otra lectura
}// Fin del void setup()

//----------Funciones de usuario

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos,
  // por ejemplo, cambiar el estado de un led si se recibe el mensaje true - false
  /*
    if (String(topic) == "codigoiot/output") {  // Si se recibe el mensaje true, se enciende el led
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }// fin de if(messageTemp == "true")
    else if(messageTemp == "false"){ // De no recibirse el mensaje true, se busca el mensaje false para apagar el led
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }// fin de else if(messageTemp == "false")
    }// fin de if (String(topic) == "codigoiot/output")
  */
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("CodigoIoTClient")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("codigoiot/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}