/*  
Este es un ejemplo simple de un script que enciende y apaga un LED 
durante un segundo de forma alternada indefinidamente
*/

int led = D0; // LED conectado a D0

// Esta rutina corre una sola vez cuando reinicia el dispositivo
void setup()
{
   pinMode(led, OUTPUT); // Initializa D0 pin como salida
}

// Esta rutina corre indefinidamente
void loop()
{
   digitalWrite(led, HIGH);  // Enciende el LED
   delay(1000);              // Espera 1000ms = 1 segundo
   digitalWrite(led, LOW);   // Apaga el LED
   delay(1000);              // Espera 1 segundo
}