// -----------------------------------
// Controlando LEDs desde el Internet
// -----------------------------------

/* Primero, vamos a crear nuestras "abreviaturas" para los pines
Igual que en el ejercicio anterior:
led1 is D0, led2 is D7 */

int led1 = D0;
int led2 = D7;

// La última vez, sólo declaramos los pines en la función setup.
// Esta vez, también vamos a registrar nuestra función Particle

void setup()
{

   // Esta es la configuración de los pines, igual que la vez pasada
   pinMode(led1, OUTPUT);
   pinMode(led2, OUTPUT);

   // También vamos a declarar Particle.function para que podamos encender o apagar el LED desde la nube.
   Particle.function("led",ledToggle);
   // Esto dice que cuando preguntemos a la nube por la función "led", deberá usar la función ledToggle() de esta aplicación.

   // Como medida de prevención, vamos a asegurarnos de que ambos LEDs estén apagados al inicio:
   digitalWrite(led1, LOW);
   digitalWrite(led2, LOW);

}


/* La vez anterior, queríamos que el LED parpadeara continuamente.
   Ahora vamos a esperar un comando desde la nube para encender o apagar el LED
   y por ello, nuestra función loop está vacia */

void loop()
{
   // No hacemos nada
}

// Vamos a tener una increíble función que será llamada cuando exista una solicitud al API
// Esta es la función ledToggle que registramos previamente como "led" en la función Particle.function.

int ledToggle(String command) {
    /* Particle.function siempre toma un string como un argumento y regresa un entero.
       Como podemos pasarle un string, significa que podemos darle los comandos de cómo deberá ser usada la función.
       En este caso, diciéndole que el comando "on" encenderá el LED y que el comando "off" lo apagará.
       Entonces, la función nos regresará un valor para dejarnos saber qué ocurrió.

       En este caso, la función regresará 1 cuando el LED se encienda, 0 cuando el LED se apague,
       y -1 si le pasamos un comando inválido y que no tiene efecto alguno sobre el LED.
    */

    if (command=="on") {
        digitalWrite(led1,HIGH);
        digitalWrite(led2,HIGH);
        return 1;
    }
    else if (command=="off") {
        digitalWrite(led1,LOW);
        digitalWrite(led2,LOW);
        return 0;
    }
    else {
        return -1;
    }
}