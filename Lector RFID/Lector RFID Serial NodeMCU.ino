// This #include statement was automatically added by the Particle IDE.
#include <MFRC522.h>

/*
----------------------------------------------------------------------------- 
 * Correspondencia de pines:
 * Signal     Pin              
 *            NodeMCU       MFRC522
 * ---------------------------------------------------------------------------
 * Reset      ANY (D2)      RST
 * SPI SS     ANY (A2)      CS
 * SPI MOSI   A5            MOSI
 * SPI MISO   A4            MISO
 * SPI SCK    A3            CLK
 */

#define SS_PIN SS //Definir (desde el compilador) que pin se van a usar para SS
#define RST_PIN D2 //Definir (desde el compilador) que pin se van a usar para RESET

MFRC522 mfrc522(SS_PIN, RST_PIN); // Crea una instancia de MFRC522   

void setup() {
    Serial.begin(9600);    // Inicia comunicación serial con la PC
    mfrc522.PCD_Init();    // Inicia la palca MFRC522
}

void loop() {
    if ( ! mfrc522.PICC_IsNewCardPresent()) { // Hay etiquetas/tarjetas nuevas?
        return; //Si no continua
    }

    // Selecciona una etiqueta/tarjeta usando ReadCardSerial, si no hay una etiqueta
    // continua
    if ( ! mfrc522.PICC_ReadCardSerial()) {
        return;
    }
    
    mfrc522.PICC_HaltA(); //Deten la lectura hasta recibir un tag nuevo
    
    String uid=""; //  Crea una variable vacia para almacenar el identificador único del tag (UID)
    
    for (byte i = 0; i < mfrc522.uid.size; i++) { // Recorre todos los bytes de UID y agregalos al string uid.
        uid =uid+mfrc522.uid.uidByte[i];
        //Envía la uid por serial
        Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        Serial.print(mfrc522.uid.uidByte[i], HEX);
    }

    //Envía la uid a la nube Particle para que la reciban todos los dispositivos subscritos
    Serial.println();
}