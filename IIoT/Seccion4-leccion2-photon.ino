int led = D0; // LED conectado al pin D0
int pushButton = D2; // Botón conectado al pin D2

// Esta rutina sólo se ejecuta una vez al inicio
void setup()
{
  pinMode(led, OUTPUT); // Inicializar el pin D0 como slaida
  pinMode(pushButton, INPUT_PULLUP); // Inicializar el pin D2 como entrada con resistencia pull-up interna.
}

// Esta rutina se ejecuta para siempre
void loop()
{
  int pushButtonState;

  pushButtonState = digitalRead(pushButton);

  if(pushButtonState == LOW){ //Si aprietas el bóton...
    digitalWrite(led, HIGH);  // Prende el led...    // y publica el evento "pushButtonState" con el dato "Pressed"
    Particle.publish("pushButtonState","Pressed",60,PRIVATE); 
    // Espera 5 segundos para evitar recibir miles de correos de IFTTT
    delay(5000);
  }
  else
  {
    digitalWrite(led, LOW);   // Apaga el LED
  }

}