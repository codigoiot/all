/*
   Sensor de presencia PIR
   Por: Hugo Vargas
   Fecha: 26 de marzo de 2020

   Este es el programa básico que realiza la lectura del sensor de presencia PIR

   Configuración de hardware
    Sensor PIR                    NodeMCU
    VCC ------------------------- 3.3v
    GND ------------------------- GND
    S   ------------------------- 1

*/

// Variables del prorgama
int pirPin = D1;  // Sensor PIR conectado a D2
int led = D0;  // Controla el led montado en el nodeMCU

//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup()
{
  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Configuración de pines
  pinMode(pirPin, INPUT); // Inicializa el pin del sensor como entrada
  pinMode(led, OUTPUT); // Inicializa el Pin del led como salida

  // Condiciones Iniciales
  digitalWrite (led, HIGH);  // Se inicia con el led apagado
}// Fin del void setup()

//----------Cuerpo del programa, bucle principal
void loop()
{
  // Variable para registrar el estado del sensor
  int pirValState;

  // Lectura del sensor
  pirValState = digitalRead(pirPin);

  if (pirValState == HIGH) { // ¿Se detectó movimiento?
    digitalWrite(led, LOW);  // Prende el LED
    Serial.println ("Se detectó movimiento");
    delay(5000); // Espera 5 segundos para hacer otra lectura
  }// Fin del if(pirValState == HIGH)
  else
  {
    digitalWrite(led, HIGH);  // Apaga el LED
    Serial.println ("No se ha detectado movimiento");
  }// fin del else
}// Fin del void setup()