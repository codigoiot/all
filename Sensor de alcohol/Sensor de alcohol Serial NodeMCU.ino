/*Lectura serial del  de alcohol
   Por: Hugo Vargas
   Fecha: 9 de marzo de 2020

   Este es un programa básico para obtener una lectura simple del 
   de alcohol, esto quiere decir que se obtendá una lectura directa del ADC.
   Obtener esta lectura no proporciona informacion sobre la concentracion
   de alcohol (mg/L). Para ello se requiere un procedimiento diferente.

   Este programa realiza promedios de 100 lecturas y envía por serial el 
   promedio de los valores leidos. Para que este programa funcione correctamente,
   es necesario que la resistencia de calentamiento del interior del 
   alcance la temperatura adecuada, esto tarda aproximadamente 30 segundos.

   Transcurrido este tiempo, se visualizará por monitor serial el valor
   de la lectura.

   Configuración de harware:
          NodeMCU
   VDD -------- 3.3V
   GND -------- GND
   A0  -------- A0

*/

// Constantes
const int WARM_UP = 30;  // Espera 30 segundos a que la resistencia de calentamiento alcance temperatura nominal
const int INTERVAL = 5000;  // Intervalo de espera entre lecturas

// Variables del programa
int sensor = A0;  // Esta variable indica el pin en el que se conecta el  de alcohol


//----------Configuración del programa, esta parte se ejecuta sólo una vez al energizarse el sistema
void setup() {
  //Configuración de pines
  pinMode(sensor, INPUT); // Se configura el pin A0 como entrada

  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};

  // Espera a que se alcance temperatura nominal de resistencia de calentamiento
  Serial.println ();
  Serial.println ("Esperando temperatura nominal en resistencia de calentamiento");
  for (int i = 0; i < WARM_UP; i++) {
    Serial.print (".");
    delay (1000);
  }
  Serial.println ();
  Serial.println ("Temperatura nominal alcanzada, ya se pueden realizar lecturas");
}// fin de void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {
  //variables
  int Value;
  //Lecturas
  for (int i = 0 ; i < 100 ; i++) //Se toman 100 lecturas para promediar su valor y obtener el valor de la lectura
  {
    Value = Value + analogRead(sensor);
  }
  Value = Value / 100.0; // Se realiza el promedio

  //Reporta por serial
  Serial.print("Lectura analogica ");
  Serial.println(Value);
  delay (INTERVAL);
}// fin del void loop ()