/* Lectura de sensor de flama
 *  Por: Hugo Vargas
 *  Fecha: 6 de marzo de 2020
 *  
 *  Este es un programa básico para obtener la lectura del sensor de audio, la
 *  cual representa el nivel de ruido presente en el entorno segun la ganancia
 *  configurada con el potenciómentro soldado sobe la placa. Las lecturas se 
 *  visualizan en el monitor serial. Para realizar la estimación de la amplitud
 *  de audio detectada, se promedia el nivel de lecturas por ensima del valor
 *  bias y el primedio del nivel de lecturas por debajo del valor  bias. La diferencia
 *  entre estos dos valores es la amplitud deportada. La forma en la que se calculan
 *  las diferencias en el programa, ayuda a garantizar que la lectura sea siempre positiva
 *  y a que siga existiendo un resultado racional en caso de que se indeterminen 
 *  los calculos de alguna mitad a causa de ligeras variaciones en el nivel de bias.
 *  
 *  Configuración del hardware;
 *  Sensor de audio      NodeMCU
 *  Vcc   -------------- 3.3V
 *  GND    -------------- GND
 *  OUT    -------------- A0
 */

// Constantes
int INTERVAL = 1000;  // Determina el tiempo entre lecturas no bloqueantes
int SAMPLES = 256;

// Variables
int audioPin = A0;  // Esta variable señala el pin donde se conecta la salida de audio
long timeNow, timeLast;  // Control de acciones asincronas

//---------Inicialización del programa, estas instrucciones se ejecutan una vez al energizar el sistema
void setup () {
  // Configuración de pines
  pinMode (audioPin, INPUT);  // Lectura de nivel de audio
  
  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
  Serial.println ("Programa iniciado");

  // Inicia seguimiento de eventos temporizados no bloqueantes
  timeLast = millis ();
}// fin del void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {

  timeNow = millis();
  if (timeNow - timeLast > INTERVAL) {
    timeLast = timeNow;  // Actualiza seguimiento de tiempo

    long posLecture = 0;
    long negLecture = 0;  //Variables que guardan los niveles de audio registrados
    long posCounter = 0;
    long negCounter = 0;  // Variables que llevan la cuenta de lecturas positivas y negativas
    
    //lectura de nivel de ruido
    for (int i = 0; i < SAMPLES; i++)  // Se promedian varias ecturas para obtener un mejor resultado
    {
      int dataAudio = analogRead (audioPin);  // Realiza la lectura analogica del microfono
      if (dataAudio > 557) {  // Si la lectura pasa del punto medio, sumar la diferencia a contador positivo
        posLecture += dataAudio - 557;
        posCounter++;
      } else {  // Si la lectura esta por debajo del punto medio, sumar la diferencia a contador negativo
        negLecture += 556 - dataAudio;
        negCounter++;
      }// fin de if (dataAudio > 512)
    }

    //Se calculan los primedips
    float avgPos = (float)posLecture/posCounter;
    float avgNeg = (float)negLecture/negCounter;
    //Se calcula la amplitud 
    float amplitude = avgPos + avgNeg;
    
    // Mostrar los datos por seial
    Serial.print ("amplitud estimada");
    Serial.println (amplitude);

  }// fin de if (timeNow - timeLast > INTERVAL)
}// fin de void loop ();