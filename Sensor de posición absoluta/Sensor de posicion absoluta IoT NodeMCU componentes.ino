/*Lectura de las componentes de sensor MPU9250 con NodeMCU CP2102
 * Por: Hugo Vargas
 * Fecha: 22 de febrero de 2020
 * 
 * Este es el programa básico de IoT con NodeMCU CP2102, MQTT y Node-Red.
 * Este programa realiza la lectura de las componentes del sensor MPU-9250
 * y las reporta por internet a traves de MQTT a un servidor en Node-Red
 * que se encarga de desplegar un tablero que reporta la información recopilada.
 * El nodeMCU además reporta su estado por comunicación serial.
 * 
 * En el nodeMCU CP2102 AMICA, los pines SDA y SCL para la comunicaicon I2C
 * se encuentran en los D2 y D1 respectivamente
 * 
 * Configuración de harware:
 * MPU-92250    NodeMCU
 * VDD -------- 3.3V
 * GND -------- GND
 * SCL -------- D1
 * SCA -------- D2
 * AD0 -------- 3.3V
 * 
*/

//Bibliotecas
#include <ESP8266WiFi.h> //Biblioteca del modulo WiFi del NodeMCU
#include <PubSubClient.h> //Biblioteca MQTT
#include "quaternionFilters.h" //Biblioteca para la obtención de los cuaterniones
#include "MPU9250.h" //Biblioteca del sensor

//Credenciales de tu red WiFi AES-2 WPA
const char* ssid = "nombre_de_red";
const char* password = "11223344";

//Dirección de Broker MQTT
const char* mqtt_server = "192.168.1.100";
IPAddress server(192,168,1,100);

//Constantes del MPU-9250
#define AHRS false                            // False para lectura de datos
#define SerialDebug true                      // Poner en true para reportes por serial
#define I2Cclock 400000                       // Frecuencia de comunicación I2C
#define I2Cport Wire                          // Indica los pines I2C
#define MPU9250_ADDRESS MPU9250_ADDRESS_AD1   // Indica la dirección I2C del sensor MPU-9250

//Objetos
WiFiClient espClient;                               // Cliente WiFi
PubSubClient client(espClient);                     // Cliente MQTT
MPU9250 myIMU(MPU9250_ADDRESS, I2Cport, I2Cclock);  // Control de variables del sensor MPU-9250

//Variables del programa
int ledPin = D0;            // Controla un pin externo
int ledPin2 = D4;
long timeNow, timeLast;     // Control de acciones asincronas

void setup() {
  // Inicialización del programa
  // Inicia comunicación serial
  Serial.begin (115200);  
  while(!Serial){};

  // Configuración de pines
  pinMode (ledPin, OUTPUT);
  pinMode (ledPin2, OUTPUT);
  digitalWrite (ledPin, HIGH);
  digitalWrite (ledPin2, HIGH);

  // Iniciar conexion WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a: ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);
  // Esparar hsata lograr conexion
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite (ledPin2, HIGH);
    delay(250);
    Serial.print(".");
    digitalWrite (ledPin2, LOW);
    delay (5);
    digitalWrite (ledPin2, HIGH);
  }
  Serial.println();
  Serial.println("WiFi conectado");
  Serial.println("Direccion IP ");
  Serial.println(WiFi.localIP());
  if (WL_CONNECTED){
  digitalWrite (ledPin2, LOW);
  }
  delay (1000);
  
  // Conexion al broker MQTT
  client.setServer(server, 1883);
  client.setCallback(callback);
  delay(1500);
  digitalWrite (ledPin, LOW);

  // Iniciar comunicación I2C
  Wire.begin();
  // Leer el registro WHO_AM_I para conocer la dirección del sensor
  byte c = myIMU.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);
  Serial.print(F("MPU9250 mi direccion es 0x"));
  Serial.print(c, HEX);
  Serial.print(F(" Y deberia ser 0x"));
  Serial.println(0x71, HEX);

  // Calibrar el sensor
  if (c == 0x71) // La dirección del sensor debe ser siempre 0x71
  {
    Serial.println(F("MPU9250 está conectado"));
    // Probar el sensor y reportar valores
    myIMU.MPU9250SelfTest(myIMU.selfTest);
    Serial.print(F("Auto test eje x: corte de aceleracion: "));
    Serial.print(myIMU.selfTest[0],1); Serial.println("% del valor de fabrica");
    Serial.print(F("Auto test eje y: corte de aceleracion: "));
    Serial.print(myIMU.selfTest[1],1); Serial.println("% del valor de fabrica");
    Serial.print(F("Auto test eje z: corte de aceleracion: "));
    Serial.print(myIMU.selfTest[2],1); Serial.println("% del valor de fabrica");
    Serial.print(F("Auto test eje x: corte de velocidad angular: "));
    Serial.print(myIMU.selfTest[3],1); Serial.println("% del valor de fabrica");
    Serial.print(F("Auto test eje y: corte de velocidad angular: "));
    Serial.print(myIMU.selfTest[4],1); Serial.println("% del valor de fabrica");
    Serial.print(F("Auto test eje z: corte de velocidad angular: "));
    Serial.print(myIMU.selfTest[5],1); Serial.println("% del valor de fabrica");
    // Calibrar giroscopio y acelerómetro segun registro de bias
    myIMU.calibrateMPU9250(myIMU.gyroBias, myIMU.accelBias);
    // Inicializar el sensor
    myIMU.initMPU9250();
    Serial.println("Modo de lectura activado");
    // Leer registro WHO_AM_I para magnetómetro
    byte d = myIMU.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
    Serial.print("La direccion del magnetometro es 0x");
    Serial.print(d, HEX);
    Serial.print(" Y debería ser 0x");
    Serial.println(0x48, HEX);
    // Detener la comunicacion si el sensor falla 
    if (d != 0x48)
    {
      Serial.println(F("Falla de comunicacion"));
      Serial.flush();
      abort();
    }
    // Calibrar el magnetómetro
    myIMU.initAK8963(myIMU.factoryMagCalibration);
    Serial.println("Modo de lectura activado para el magnetometro");
    if (SerialDebug)
    {
      Serial.print("Ajuste de sensibilidad en eje X ");
      Serial.println(myIMU.factoryMagCalibration[0], 2);
      Serial.print("Ajuste de sensibilidad en eje Y ");
      Serial.println(myIMU.factoryMagCalibration[1], 2);
      Serial.print("Ajuste de sensibilidad en eje Z ");
      Serial.println(myIMU.factoryMagCalibration[2], 2);
    }

    // Obrener resolución de los sensores
    myIMU.getAres();
    myIMU.getGres();
    myIMU.getMres();

    // La siguiente instrucción graba 15 segundos de lecturas para calcular el Bias
    //myIMU.magCalMPU9250(myIMU.magBias, myIMU.magScale);
    Serial.println("Bias del magnetometro (mG)");
    Serial.println(myIMU.magBias[0]);
    Serial.println(myIMU.magBias[1]);
    Serial.println(myIMU.magBias[2]);

    Serial.println("Escala del magnetometro (mG)");
    Serial.println(myIMU.magScale[0]);
    Serial.println(myIMU.magScale[1]);
    Serial.println(myIMU.magScale[2]);
    //delay(2000); // Agregar un delay antes de mostrar registros

    if(SerialDebug)
    {
      Serial.println("Magnetometer:");
      Serial.print("Ajuste de sensibilidad en eje X ");
      Serial.println(myIMU.factoryMagCalibration[0], 2);
      Serial.print("Ajuste de sensibilidad en eje Y ");
      Serial.println(myIMU.factoryMagCalibration[1], 2);
      Serial.print("Ajuste de sensibilidad en eje Z ");
      Serial.println(myIMU.factoryMagCalibration[2], 2);
    }
  } // Si la dirección del sensor no es 0x71, entonces mandar error de comunicación
  else
  {
    Serial.print("No se pudo establecer comuniacion con el sensor, su direcicon es 0x");
    Serial.println(c, HEX);
    Serial.println(F("Error de comunicacion, abortano!"));
    Serial.flush();
    abort();
  }    
}// Fin de void setup ()

// Funcion Callback, aqui se pueden poner acciones si se reciben mensajes MQTT
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Llego mensaje en el tema ");
  Serial.print(topic);
  Serial.print(". El mensaje es: ");
  // Imprimir mensaje
  String messageTemp;  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Aqui puedes agregar mas acciones y comparaciones con temas o mensajes específicos

  // Cambiar el estado de un led si se recibe el mensaje true - false
  if (String(topic) == "esp32/output") {
    Serial.print("Se recibio ");
    if(messageTemp == "true"){
      Serial.println("true");
      digitalWrite(ledPin, LOW);
    }
    else if(messageTemp == "false"){
      Serial.println("false");
      digitalWrite(ledPin, HIGH);
    }
  }
}

// funcion para reconectar el cliente MQTT
void reconnect() {
  // Bucle hasta que se logre la conexión
  while (!client.connected()) {
    Serial.print("Intentando conexion MQTT");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conexion exitosa");
      // Suscribirse al tema de respuestas
      client.subscribe("esp32/output");
      digitalWrite(ledPin, LOW);
    } else {
      digitalWrite(ledPin, HIGH);
      Serial.print("Falló la comunicación, error rc=");
      Serial.print(client.state());
      Serial.println(" Intentar de nuevo en 6 segundos");
      delay(6000);
      Serial.println (client.connected ());
    }
  }
}

//Cuerpo del programa, bucle principal
void loop() {
  // Comprobar conexion con broker MQTT
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // Funciones temporizadas no bloqueantes
  timeNow = millis();
  if (timeNow - timeLast > 500) {
    timeLast = timeNow;

    //Lectura y calculo de los valores a reportar
  if (myIMU.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)
  {
    myIMU.readAccelData(myIMU.accelCount);  // Leer acelerometro
    // Calcular la aceleracion en gravedades
    // Depende de la escala configurada, descomentar el final para ajuste contra bias
    myIMU.ax = (float)myIMU.accelCount[0] * myIMU.aRes; // - myIMU.accelBias[0];
    myIMU.ay = (float)myIMU.accelCount[1] * myIMU.aRes; // - myIMU.accelBias[1];
    myIMU.az = (float)myIMU.accelCount[2] * myIMU.aRes; // - myIMU.accelBias[2];

    myIMU.readGyroData(myIMU.gyroCount);  // Leer giroscopio
    // Calcular la velocidad angular del giroscopio
    // Depende de la escala configurada
    myIMU.gx = (float)myIMU.gyroCount[0] * myIMU.gRes;
    myIMU.gy = (float)myIMU.gyroCount[1] * myIMU.gRes;
    myIMU.gz = (float)myIMU.gyroCount[2] * myIMU.gRes;

    myIMU.readMagData(myIMU.magCount);  // Leer el magnetometro

    // Cacular los valores del magnetometro en milliGauss
    // Incluye la calibracion de fabrica
    myIMU.mx = (float)myIMU.magCount[0] * myIMU.mRes * myIMU.factoryMagCalibration[0] - myIMU.magBias[0];
    myIMU.my = (float)myIMU.magCount[1] * myIMU.mRes * myIMU.factoryMagCalibration[1] - myIMU.magBias[1];
    myIMU.mz = (float)myIMU.magCount[2] * myIMU.mRes * myIMU.factoryMagCalibration[2] - myIMU.magBias[2];
  }
   
  // Debe ser llamado antes de actualizar quaterniones
  myIMU.updateTime();
  
  // Para obtener los datos se usará como ejemplo el filtro Mahony, el cual despliega
  // la información de las componentes a partir de la lectura del quaternion que entrega
  // el sensor. Las relaciones siguientes son determinadas por la oritentacion
  // interna de los sensores, donde el plano x (y) del acelerometro esta alineado
  // con el plano y (x) del magnetometro y  el eje z del magnetómetro esta alineado
  // en oposicion  al eje z del acelerometro y el giroscopio.
  MahonyQuaternionUpdate(myIMU.ax, myIMU.ay, myIMU.az, myIMU.gx * DEG_TO_RAD,
                         myIMU.gy * DEG_TO_RAD, myIMU.gz * DEG_TO_RAD, myIMU.my,
                         myIMU.mx, myIMU.mz, myIMU.deltat);
  }

  //En caso de lectura de datos normal
  if (!AHRS)
  {
    myIMU.delt_t = millis() - myIMU.count;//Realizar las lecturas de manera asincrona
    if (myIMU.delt_t > 500)
    {
      if(SerialDebug)
      {
        // Imprimir aceleracion en mili gravedades
        Serial.print("Aceleracion en X: ");
        Serial.print(1000 * myIMU.ax);
        Serial.print(" mG \t\t");
        Serial.print("Aceleracion en Y: ");
        Serial.print(1000 * myIMU.ay);
        Serial.print(" mG \t\t");
        Serial.print("Aceleracion en z: ");
        Serial.print(1000 * myIMU.az);
        Serial.println(" mG");

        // Imprimir valores del giroscopio en grados/segundo
        Serial.print("Giroscopio en X: ");
        Serial.print(myIMU.gx, 3);
        Serial.print(" °/S \t\t");
        Serial.print("Giroscopio en Y: ");
        Serial.print(myIMU.gy, 3);
        Serial.print(" °/S \t\t");
        Serial.print("Giroscopio en Z: ");
        Serial.print(myIMU.gz, 3);
        Serial.println(" °/S");

        // Imprimir valores del acelerómetro en grados/segundo
        Serial.print("Magnetometro en X: ");
        Serial.print(myIMU.mx);
        Serial.print(" mG \t\t");
        Serial.print("Magnetometro en X: ");
        Serial.print(myIMU.my);
        Serial.print(" mG \t\t");
        Serial.print("Magnetometro en X: ");
        Serial.print(myIMU.mz);
        Serial.println(" mG");

        myIMU.tempCount = myIMU.readTempData();  // Leer valores de temperatura y pasar a grados celcius
        myIMU.temperature = ((float) myIMU.tempCount) / 333.87 + 21.0;
        Serial.print("La temperatura es ");
        Serial.print(myIMU.temperature, 1);
        Serial.println(" °C");

        // Envio por MQTT
        char dataString [8];
        dtostrf (1000 * myIMU.ax, 1, 2, dataString);
        client.publish("esp32/accx", dataString);
        dtostrf (1000 * myIMU.ay, 1, 2, dataString);
        client.publish("esp32/accy", dataString);
        dtostrf (1000 * myIMU.az, 1, 2, dataString);
        client.publish("esp32/accz", dataString);
        dtostrf (myIMU.gx, 1, 2, dataString);
        client.publish("esp32/gyrx", dataString);
        dtostrf (myIMU.gy, 1, 2, dataString);
        client.publish("esp32/gyry", dataString);
        dtostrf (myIMU.gz, 1, 2, dataString);
        client.publish("esp32/gyrz", dataString);
        dtostrf (myIMU.mx, 1, 2, dataString);
        client.publish("esp32/magx", dataString);
        dtostrf (myIMU.my, 1, 2, dataString);
        client.publish("esp32/magy", dataString);
        dtostrf (myIMU.mz, 1, 2, dataString);
        client.publish("esp32/magz", dataString);
        dtostrf (myIMU.temperature, 1, 2, dataString);
        client.publish("esp32/temp", dataString);
      }

      myIMU.count = millis();
      digitalWrite(ledPin, !digitalRead(ledPin));  // Cambiar el estado del led
    } // if (myIMU.delt_t > 500)
  } // if (!AHRS)

}