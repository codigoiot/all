//Internet de las cosas para la industria
//Modulo 15 Consumo Eléctrico

int sensorTA12 = A0; // Pin al que se conecta el sensor
float nVPP;   // Voltaje en la resistencia
float nCurrThruResistorPP; // Corriente pico en la resistencia
float nCurrThruResistorRMS; // RMS en la resistencia
float nCurrentThruWire;     // RMS en el cable
double nPower;     // Potencia de consumo
float consumedPower=0; //Suma del consumo
float consumedkWh; //Consumo en kWh
float pricekWh=3.332; //Precio promedio del kWh de CFE (en pesos)
double operationCost; //Costo de operación del aparato
double averageConsumption; //Consumo promedio
int samples=0; //Numero de mediciones

void setup() 
 {
   Serial.begin(9600); 
   pinMode(sensorTA12, INPUT);
   //Definicion de variables en linea
   Particle.variable("potencia", nPower);
   Particle.variable("costo", operationCost);
   Particle.variable("promedio", averageConsumption);
 }
 
 
 void loop() 
 {
   
   
   nVPP = getVPP();  nCurrThruResistorPP = (nVPP/800.0) * 1000.0;
   
   /* 
   Calcula la corriente RMS usando la formula
   para una onda sinusoidal.
   */
   
   nCurrThruResistorRMS = nCurrThruResistorPP * 0.707;
   
   /* 
   El transformador tiene proporcion de 2000:1...
   Por lo tanto la corriente en la  resistencia
   de 800 ohm se multiplica por 2000 para tener
   la corriente en el cable.
   */
   
   nCurrentThruWire = nCurrThruResistorRMS * 2000;
   
   /*
   
   /*
   Calcula la corriente (en mA) en la resistencia usando
   ley de ohm.
   */
   
   /*
    La conversión a RMS permite usar cálculos de corriente
   directa de manera que la potencia(p)=voltaje(v)*corriente(i.
   Para 120VAC P=120*nCurrentThruWire. Divide entre 1000 para
   obtener amperes.
   */
   nPower=120*(nCurrentThruWire/1000);
   
   /*
    Sumamos la potencia utilizada desde el arranque hasta
    ahora.
   */
   consumedPower+=nPower;
   
   /*
   Dividimos la sumatoria de las mediciones entre el
   número de mediciones para obtener un consumo promedio.
   */
   averageConsumption=consumedPower/samples;
   
   /*
   Para convertir a kWh divide entre 1000 para obtener kW y
   multiplica por el número de horas por las que has estado
   midiendo (millisegundos entre 3600000)
   And to convert it into kWh we convert to kW (divide by 1000)
   and multiply by the amount of hours the measuring has ocurred 
   for (milliseconds / 3600000).
   */
   float seconds=millis()/1000;
   float hours=seconds/3600;
   consumedkWh=(averageConsumption/1000)*hours;
   
   /*
    Multiplica el consumo por el costo del kWh en tu zona
   */
   operationCost=consumedkWh*pricekWh;
   
   /*
   Redondear a dos digitos decimales los valores para
   mostrarlos.
   */
   nPower=round(nPower*100)/100;
   operationCost=round(operationCost*100)/100;
   averageConsumption=round(averageConsumption*100)/100;
   
   //Muestra los cálculos
   Serial.print("Mediciones: ");
   Serial.println(samples);

   Serial.print("Voltaje pico : ");
   Serial.println(nVPP,3);
 
   Serial.print("Corriente en la resistencia (pico) : ");
   Serial.print(nCurrThruResistorPP,3);
   Serial.println(" mA Pico a Pico");
   
   Serial.print("Corriente en la resistencia (RMS) : ");
   Serial.print(nCurrThruResistorRMS,3);
   Serial.println(" mA RMS");
   
   Serial.print("Corriente en el cable : ");
   Serial.print(nCurrentThruWire,3);
   Serial.println(" mA RMS");
   
   Serial.print("Potencia RMS (Watts): ");
   Serial.print(nPower,3);
   Serial.println(" W RMS");
   
   Serial.print("Potencia promedio : ");
   Serial.print(averageConsumption,3);
   Serial.println(" W RMS");
   
   Serial.print("kWh usados: ");
   Serial.print(consumedkWh,3);
   Serial.println(" kWh RMS");
   
   Serial.print("Costo de operación: $");
   Serial.print(operationCost,3);
   Serial.println(" MXN");
   
   Serial.println();
    
 }


 /*************************************
Para calcular la corriente RMS necesitas saber el 
voltaje de pico a pico medido en la resistencia
de 800 Ohms.

La siguiente función toma un segundo de mediciones y 
regresa el valor pico en ese intervalo.
 *************************************/
 
float getVPP()
{
  float result;
  int readValue;             //lectura del sensor
  int maxValue = 0;          //aquí se almacena el máximo
   uint32_t start_time = millis();
   while((millis()-start_time) < 1000) //tomar un segundo de mediciones
   {
       readValue = analogRead(sensorTA12);//leer el sensor
       //readValue = rand() % (1200) + 800;
       // revisar si hay un máximo nuevo
       if (readValue > maxValue) 
       {
           //Guardar el máximo
           maxValue = readValue;
       }
   }
   
   // Convierte la lectura digital a un voltaje
   result = (maxValue * 3.3)/4096.0;
   samples++;
   return result;
 }
 