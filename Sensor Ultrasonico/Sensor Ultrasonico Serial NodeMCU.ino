/* Lectura de sensor de distancia utlrasónico
 *  Por: Hugo Vargas
 *  Fecha: 4 de marzo de 2020
 *  
 *  Este es un programa básico para obtener una lectura simple del sensor de distancia
 *  ultrasónicom esto quiere decir que se obtendra el valor de la distancia a la que 
 *  se encuentra un obstaculo frente al sensor.
 *  
 *  Este programa manda un puso de 10us y después escucha el pulso de respuesta del
 *  sensor, el cual tiene misma duración que el tiempo que tomo al eco regresar. Después
 *  se calcula la distancia a la que se encuentra el obstaculo y se envía por serial.
 *  
 *  Configuración del hardware;
 *  Sensor Ultrasónico  NodeMCU
 *  Vcc   -------------- 3.3V
 *  Trig  -------------- D1
 *  Echo  -------------- D2
 *  GND   -------------- GND
 */

// Variables
int trig = D1;  // Representa el pin con el que se activa el sensor
int echo = D2;  // Representa el pin en el que se lle a respuesta del sensor
long t;  // Variable que almacena el tiempo que tarda en regesar el eco
long d;  // Variable que almacena el resultado del calculo de la distancia

//---------Inicialización del programa, estas instrucciones se ejecutan una vez al energizar el sistema
void setup () {
  // Configuración de pines
  pinMode (trig, OUTPUT);  // Por el pin trig sale una señal eléctrica
  pinMode (echo, INPUT);  // Por el pin echo se recibe una señal eléctrica

  // Inicia comunicación serial
  Serial.begin (115200);
  while (!Serial) {};
  Serial.println ("Programa iniciado");
}// fin del void setup ()

//----------Cuerpo del programa, bucle principal
void loop() {
  digitalWrite (trig, HIGH);  // Se lanza el pulso que activa el sensor
  delayMicroseconds (10);  // Este pulso debe tener una duración de 10 us
  digitalWrite (trig, LOW);  // Se desactiva el pin para finalizar el pulso

  //Medir el tiempo que tarda en regresar el eco
  t = pulseIn (echo, HIGH);  // Se almacena en t el valor de duración del pulso
                             // Se hace con ayuda de la función pulseIn, la cual
                             // forma parte de las bibliotecas de arduino, cuenta
                             // con dos parámetros, el primero es el pin que se
                             // va a leer, el segundo es el tipo de pulsos que
                             // detecta y puede ser HIGH o LOW

  d = t/58.275;  // Esta formula obtiene la distancia al multiplicar por el factor
                   // de conversión de la velocidad del sonido en m/s a cm/uS para
                   // una velocidad del sonido de 343.2m/s

  //mandar reporte por monitor serial
  Serial.print ("Eco ");
  Serial.print (t);
  Serial.println ("us");
  Serial.print ("Distancia ");
  Serial.print (d);
  Serial.println ("cm");
  delay (1000);  // Espera para hacer legibles las lecturas
}// fin de void loop ();